-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jul 26, 2018 at 02:35 AM
-- Server version: 5.6.38
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zmc_app_base`
--

-- --------------------------------------------------------

--
-- Table structure for table `acl_phinxlog`
--

CREATE TABLE `acl_phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acl_phinxlog`
--

INSERT INTO `acl_phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`) VALUES
(20141229162641, 'CakePhpDbAcl', '2018-02-01 03:04:00', '2018-02-01 03:04:01', 0);

-- --------------------------------------------------------

--
-- Table structure for table `acos`
--

CREATE TABLE `acos` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(11) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `sort` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acos`
--

INSERT INTO `acos` (`id`, `parent_id`, `name`, `model`, `foreign_key`, `alias`, `lft`, `rght`, `status`, `sort`) VALUES
(1, NULL, 'controllers', NULL, NULL, 'controllers', 1, 94, 0, 0),
(2, 1, 'Error', NULL, NULL, 'Error', 2, 3, 1, 0),
(3, 1, 'Pages', NULL, NULL, 'Pages', 4, 15, 1, 0),
(19, 1, 'Acl', NULL, NULL, 'Acl', 16, 17, 1, 0),
(20, 1, 'Bake', NULL, NULL, 'Bake', 18, 19, 1, 0),
(21, 1, 'DebugKit', NULL, NULL, 'DebugKit', 20, 47, 1, 0),
(22, 21, 'Composer', NULL, NULL, 'Composer', 21, 24, 1, 0),
(23, 22, 'checkDependencies', NULL, NULL, 'checkDependencies', 22, 23, 1, 0),
(24, 21, 'MailPreview', NULL, NULL, 'MailPreview', 25, 32, 1, 0),
(25, 24, 'index', NULL, NULL, 'index', 26, 27, 1, 0),
(26, 24, 'sent', NULL, NULL, 'sent', 28, 29, 1, 0),
(27, 24, 'email', NULL, NULL, 'email', 30, 31, 1, 0),
(28, 21, 'Panels', NULL, NULL, 'Panels', 33, 38, 1, 0),
(29, 28, 'index', NULL, NULL, 'index', 34, 35, 1, 0),
(30, 28, 'view', NULL, NULL, 'view', 36, 37, 1, 4),
(31, 21, 'Requests', NULL, NULL, 'Requests', 39, 42, 1, 0),
(32, 31, 'view', NULL, NULL, 'view', 40, 41, 1, 4),
(33, 21, 'Toolbar', NULL, NULL, 'Toolbar', 43, 46, 1, 0),
(34, 33, 'clearCache', NULL, NULL, 'clearCache', 44, 45, 1, 0),
(35, 1, 'Migrations', NULL, NULL, 'Migrations', 48, 49, 1, 0),
(56, 1, 'AuditStash', NULL, NULL, 'AuditStash', 50, 51, 1, 0),
(59, 1, 'Josegonzalez\\Upload', NULL, NULL, 'Josegonzalez\\Upload', 52, 53, 1, 0),
(60, 1, 'AppSettings', NULL, NULL, 'AppSettings', 54, 57, 0, 100),
(61, 60, 'index', NULL, NULL, 'index', 55, 56, 0, 0),
(62, 1, 'Dashboard', NULL, NULL, 'Dashboard', 58, 61, 0, 0),
(63, 62, 'index', NULL, NULL, 'index', 59, 60, 0, 0),
(64, 1, 'Errors', NULL, NULL, 'Errors', 62, 65, 1, 0),
(65, 64, 'unauthorized', NULL, NULL, 'unauthorized', 63, 64, 1, 0),
(66, 1, 'Groups', NULL, NULL, 'Groups', 66, 79, 0, 66),
(67, 66, 'index', NULL, NULL, 'index', 67, 68, 0, 0),
(68, 66, 'view', NULL, NULL, 'view', 69, 70, 0, 4),
(69, 66, 'add', NULL, NULL, 'add', 71, 72, 0, 1),
(70, 66, 'edit', NULL, NULL, 'edit', 73, 74, 0, 2),
(71, 66, 'delete', NULL, NULL, 'delete', 75, 76, 0, 3),
(72, 66, 'configure', NULL, NULL, 'configure', 77, 78, 1, 5),
(73, 3, 'index', NULL, NULL, 'index', 5, 6, 1, 0),
(74, 3, 'logout', NULL, NULL, 'logout', 7, 8, 1, 0),
(75, 3, 'editProfile', NULL, NULL, 'editProfile', 9, 10, 1, 0),
(76, 3, 'activitiesLog', NULL, NULL, 'activitiesLog', 11, 12, 1, 0),
(77, 1, 'Users', NULL, NULL, 'Users', 80, 93, 0, 67),
(78, 77, 'index', NULL, NULL, 'index', 81, 82, 0, 0),
(79, 77, 'view', NULL, NULL, 'view', 83, 84, 0, 4),
(80, 77, 'add', NULL, NULL, 'add', 85, 86, 0, 1),
(81, 77, 'edit', NULL, NULL, 'edit', 87, 88, 0, 2),
(82, 77, 'delete', NULL, NULL, 'delete', 89, 90, 0, 3),
(83, 77, 'configure', NULL, NULL, 'configure', 91, 92, 0, 5),
(89, 3, NULL, NULL, NULL, 'uploadMedia', 13, 14, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `app_settings`
--

CREATE TABLE `app_settings` (
  `id` int(11) NOT NULL,
  `keyField` varchar(225) NOT NULL,
  `valueField` varchar(225) NOT NULL,
  `type` enum('text','long text','image','select') NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_settings`
--

INSERT INTO `app_settings` (`id`, `keyField`, `valueField`, `type`, `status`) VALUES
(1, 'App.Name', 'Z-APP-BASE', 'text', 0),
(2, 'App.Logo', '/webroot/assets/img/logo.png', 'image', 0),
(3, 'App.Logo.Login', '/webroot/assets/img/logo_login.png', 'image', 0),
(4, 'App.Logo.Width', '160', 'text', 0),
(5, 'App.Logo.Height', '80', 'text', 0),
(6, 'App.Logo.Login.Width', '160', 'text', 0),
(7, 'App.Logo.Login.Height', '160', 'text', 0),
(8, 'App.Login.Cover', '/webroot/assets/img/cover_login.jpg', 'image', 0),
(9, 'App.Description', 'Management application', 'long text', 0),
(10, 'App.Favico', '/webroot/assets/img/favico.png', 'long text', 0);

-- --------------------------------------------------------

--
-- Table structure for table `aros`
--

CREATE TABLE `aros` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(11) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aros`
--

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, 'Groups', 1, NULL, 1, 4),
(2, 1, 'Users', 1, NULL, 2, 3),
(5, NULL, 'Groups', 4, NULL, 5, 6);

-- --------------------------------------------------------

--
-- Table structure for table `aros_acos`
--

CREATE TABLE `aros_acos` (
  `id` int(11) NOT NULL,
  `aro_id` int(11) NOT NULL,
  `aco_id` int(11) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aros_acos`
--

INSERT INTO `aros_acos` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`) VALUES
(1, 1, 6, '1', '1', '1', '1'),
(2, 1, 7, '1', '1', '1', '1'),
(3, 1, 8, '1', '1', '1', '1'),
(4, 1, 9, '1', '1', '1', '1'),
(5, 1, 10, '1', '1', '1', '1'),
(6, 1, 11, '1', '1', '1', '1'),
(7, 1, 12, '1', '1', '1', '1'),
(8, 1, 13, '1', '1', '1', '1'),
(9, 1, 14, '1', '1', '1', '1'),
(10, 1, 15, '1', '1', '1', '1'),
(11, 1, 16, '1', '1', '1', '1'),
(12, 1, 17, '1', '1', '1', '1'),
(13, 1, 18, '1', '1', '1', '1'),
(14, 2, 6, '1', '1', '1', '1'),
(15, 2, 7, '1', '1', '1', '1'),
(16, 2, 8, '1', '1', '1', '1'),
(17, 2, 9, '1', '1', '1', '1'),
(18, 2, 13, '1', '1', '1', '1'),
(19, 2, 14, '1', '1', '1', '1'),
(20, 2, 15, '1', '1', '1', '1'),
(21, 2, 16, '1', '1', '1', '1'),
(22, 2, 17, '1', '1', '1', '1'),
(23, 2, 18, '1', '1', '1', '1'),
(24, 2, 10, '1', '1', '1', '1'),
(25, 2, 11, '1', '1', '1', '1'),
(26, 2, 12, '1', '1', '1', '1'),
(57, 1, 36, '1', '1', '1', '1'),
(58, 1, 37, '1', '1', '1', '1'),
(59, 2, 36, '1', '1', '1', '1'),
(60, 1, 40, '1', '1', '1', '1'),
(61, 2, 40, '1', '1', '1', '1'),
(62, 2, 37, '1', '1', '1', '1'),
(63, 1, 47, '1', '1', '1', '1'),
(64, 1, 49, '1', '1', '1', '1'),
(65, 1, 48, '1', '1', '1', '1'),
(66, 1, 52, '1', '1', '1', '1'),
(67, 1, 51, '1', '1', '1', '1'),
(68, 1, 50, '1', '1', '1', '1'),
(69, 1, 41, '1', '1', '1', '1'),
(70, 1, 45, '1', '1', '1', '1'),
(71, 1, 44, '1', '1', '1', '1'),
(72, 1, 43, '1', '1', '1', '1'),
(73, 1, 42, '1', '1', '1', '1'),
(74, 1, 46, '1', '1', '1', '1'),
(75, 2, 47, '1', '1', '1', '1'),
(76, 2, 49, '1', '1', '1', '1'),
(77, 2, 48, '1', '1', '1', '1'),
(78, 2, 52, '1', '1', '1', '1'),
(79, 2, 51, '1', '1', '1', '1'),
(80, 2, 50, '1', '1', '1', '1'),
(81, 2, 41, '1', '1', '1', '1'),
(82, 2, 45, '1', '1', '1', '1'),
(83, 2, 44, '1', '1', '1', '1'),
(84, 2, 43, '1', '1', '1', '1'),
(85, 2, 42, '1', '1', '1', '1'),
(86, 2, 46, '1', '1', '1', '1'),
(87, 1, 57, '1', '1', '1', '1'),
(88, 1, 58, '1', '1', '1', '1'),
(89, 2, 57, '1', '1', '1', '1'),
(90, 2, 58, '1', '1', '1', '1'),
(121, 1, 62, '1', '1', '1', '1'),
(122, 1, 63, '1', '1', '1', '1'),
(123, 1, 66, '1', '1', '1', '1'),
(124, 1, 68, '1', '1', '1', '1'),
(125, 1, 67, '1', '1', '1', '1'),
(126, 1, 71, '1', '1', '1', '1'),
(127, 1, 70, '1', '1', '1', '1'),
(128, 1, 69, '1', '1', '1', '1'),
(129, 1, 77, '1', '1', '1', '1'),
(130, 1, 82, '1', '1', '1', '1'),
(131, 1, 81, '1', '1', '1', '1'),
(132, 1, 80, '1', '1', '1', '1'),
(133, 1, 79, '1', '1', '1', '1'),
(134, 1, 83, '1', '1', '1', '1'),
(135, 1, 78, '1', '1', '1', '1'),
(136, 1, 60, '1', '1', '1', '1'),
(137, 1, 61, '1', '1', '1', '1'),
(138, 2, 62, '1', '1', '1', '1'),
(139, 2, 63, '1', '1', '1', '1'),
(140, 2, 66, '1', '1', '1', '1'),
(141, 2, 68, '1', '1', '1', '1'),
(142, 2, 67, '1', '1', '1', '1'),
(143, 2, 71, '1', '1', '1', '1'),
(144, 2, 70, '1', '1', '1', '1'),
(145, 2, 69, '1', '1', '1', '1'),
(146, 2, 77, '1', '1', '1', '1'),
(147, 2, 82, '1', '1', '1', '1'),
(148, 2, 81, '1', '1', '1', '1'),
(149, 2, 80, '1', '1', '1', '1'),
(150, 2, 79, '1', '1', '1', '1'),
(151, 2, 83, '1', '1', '1', '1'),
(152, 2, 78, '1', '1', '1', '1'),
(153, 2, 60, '1', '1', '1', '1'),
(154, 2, 61, '1', '1', '1', '1'),
(196, 1, 97, '1', '1', '1', '1'),
(197, 1, 98, '1', '1', '1', '1'),
(198, 1, 99, '1', '1', '1', '1'),
(199, 1, 100, '1', '1', '1', '1'),
(200, 1, 101, '1', '1', '1', '1'),
(202, 2, 97, '1', '1', '1', '1'),
(203, 2, 98, '1', '1', '1', '1'),
(204, 2, 99, '1', '1', '1', '1'),
(205, 2, 100, '1', '1', '1', '1'),
(206, 2, 101, '1', '1', '1', '1'),
(208, 1, 117, '1', '1', '1', '1'),
(209, 1, 119, '1', '1', '1', '1'),
(210, 1, 120, '1', '1', '1', '1'),
(211, 1, 121, '1', '1', '1', '1'),
(212, 1, 118, '1', '1', '1', '1'),
(214, 2, 117, '1', '1', '1', '1'),
(215, 2, 119, '1', '1', '1', '1'),
(216, 2, 120, '1', '1', '1', '1'),
(217, 2, 121, '1', '1', '1', '1'),
(218, 2, 118, '1', '1', '1', '1'),
(220, 1, 134, '1', '1', '1', '1'),
(221, 1, 138, '1', '1', '1', '1'),
(222, 1, 137, '1', '1', '1', '1'),
(223, 1, 136, '1', '1', '1', '1'),
(224, 1, 135, '1', '1', '1', '1'),
(226, 2, 134, '1', '1', '1', '1'),
(227, 2, 138, '1', '1', '1', '1'),
(228, 2, 137, '1', '1', '1', '1'),
(229, 2, 136, '1', '1', '1', '1'),
(230, 2, 135, '1', '1', '1', '1'),
(232, 1, 147, '1', '1', '1', '1'),
(233, 1, 149, '1', '1', '1', '1'),
(234, 1, 150, '1', '1', '1', '1'),
(235, 1, 151, '1', '1', '1', '1'),
(236, 1, 148, '1', '1', '1', '1'),
(238, 1, 165, '1', '1', '1', '1'),
(239, 1, 167, '1', '1', '1', '1'),
(240, 1, 168, '1', '1', '1', '1'),
(241, 1, 169, '1', '1', '1', '1'),
(242, 1, 166, '1', '1', '1', '1'),
(244, 1, 153, '1', '1', '1', '1'),
(245, 1, 155, '1', '1', '1', '1'),
(246, 1, 156, '1', '1', '1', '1'),
(247, 1, 157, '1', '1', '1', '1'),
(248, 1, 154, '1', '1', '1', '1'),
(250, 1, 141, '1', '1', '1', '1'),
(251, 1, 143, '1', '1', '1', '1'),
(252, 1, 144, '1', '1', '1', '1'),
(253, 1, 145, '1', '1', '1', '1'),
(254, 1, 142, '1', '1', '1', '1'),
(256, 1, 159, '1', '1', '1', '1'),
(257, 1, 161, '1', '1', '1', '1'),
(258, 1, 162, '1', '1', '1', '1'),
(259, 1, 163, '1', '1', '1', '1'),
(260, 1, 160, '1', '1', '1', '1'),
(262, 1, 183, '1', '1', '1', '1'),
(263, 1, 185, '1', '1', '1', '1'),
(264, 1, 186, '1', '1', '1', '1'),
(265, 1, 187, '1', '1', '1', '1'),
(266, 1, 184, '1', '1', '1', '1'),
(268, 1, 171, '1', '1', '1', '1'),
(269, 1, 173, '1', '1', '1', '1'),
(270, 1, 174, '1', '1', '1', '1'),
(271, 1, 175, '1', '1', '1', '1'),
(272, 1, 172, '1', '1', '1', '1'),
(274, 1, 177, '1', '1', '1', '1'),
(275, 1, 179, '1', '1', '1', '1'),
(276, 1, 180, '1', '1', '1', '1'),
(277, 1, 181, '1', '1', '1', '1'),
(278, 1, 178, '1', '1', '1', '1'),
(280, 2, 147, '1', '1', '1', '1'),
(281, 2, 149, '1', '1', '1', '1'),
(282, 2, 150, '1', '1', '1', '1'),
(283, 2, 151, '1', '1', '1', '1'),
(284, 2, 148, '1', '1', '1', '1'),
(286, 2, 165, '1', '1', '1', '1'),
(287, 2, 167, '1', '1', '1', '1'),
(288, 2, 168, '1', '1', '1', '1'),
(289, 2, 169, '1', '1', '1', '1'),
(290, 2, 166, '1', '1', '1', '1'),
(292, 2, 153, '1', '1', '1', '1'),
(293, 2, 155, '1', '1', '1', '1'),
(294, 2, 156, '1', '1', '1', '1'),
(295, 2, 157, '1', '1', '1', '1'),
(296, 2, 154, '1', '1', '1', '1'),
(298, 2, 141, '1', '1', '1', '1'),
(299, 2, 143, '1', '1', '1', '1'),
(300, 2, 144, '1', '1', '1', '1'),
(301, 2, 145, '1', '1', '1', '1'),
(302, 2, 142, '1', '1', '1', '1'),
(304, 2, 159, '1', '1', '1', '1'),
(305, 2, 161, '1', '1', '1', '1'),
(306, 2, 162, '1', '1', '1', '1'),
(307, 2, 163, '1', '1', '1', '1'),
(308, 2, 160, '1', '1', '1', '1'),
(310, 2, 183, '1', '1', '1', '1'),
(311, 2, 185, '1', '1', '1', '1'),
(312, 2, 186, '1', '1', '1', '1'),
(313, 2, 187, '1', '1', '1', '1'),
(314, 2, 184, '1', '1', '1', '1'),
(316, 2, 171, '1', '1', '1', '1'),
(317, 2, 173, '1', '1', '1', '1'),
(318, 2, 174, '1', '1', '1', '1'),
(319, 2, 175, '1', '1', '1', '1'),
(320, 2, 172, '1', '1', '1', '1'),
(322, 2, 177, '1', '1', '1', '1'),
(323, 2, 179, '1', '1', '1', '1'),
(324, 2, 180, '1', '1', '1', '1'),
(325, 2, 181, '1', '1', '1', '1'),
(326, 2, 178, '1', '1', '1', '1'),
(328, 1, 197, '1', '1', '1', '1'),
(329, 1, 199, '1', '1', '1', '1'),
(330, 1, 200, '1', '1', '1', '1'),
(331, 1, 201, '1', '1', '1', '1'),
(332, 1, 198, '1', '1', '1', '1'),
(334, 1, 191, '1', '1', '1', '1'),
(335, 1, 193, '1', '1', '1', '1'),
(336, 1, 194, '1', '1', '1', '1'),
(337, 1, 195, '1', '1', '1', '1'),
(338, 1, 192, '1', '1', '1', '1'),
(340, 2, 197, '1', '1', '1', '1'),
(341, 2, 199, '1', '1', '1', '1'),
(342, 2, 200, '1', '1', '1', '1'),
(343, 2, 201, '1', '1', '1', '1'),
(344, 2, 198, '1', '1', '1', '1'),
(346, 2, 191, '1', '1', '1', '1'),
(347, 2, 193, '1', '1', '1', '1'),
(348, 2, 194, '1', '1', '1', '1'),
(349, 2, 195, '1', '1', '1', '1'),
(350, 2, 192, '1', '1', '1', '1'),
(352, 1, 204, '1', '1', '1', '1'),
(353, 1, 205, '1', '1', '1', '1'),
(354, 1, 206, '1', '1', '1', '1'),
(355, 1, 207, '1', '1', '1', '1'),
(356, 1, 208, '1', '1', '1', '1'),
(358, 2, 204, '1', '1', '1', '1'),
(359, 2, 205, '1', '1', '1', '1'),
(360, 2, 206, '1', '1', '1', '1'),
(361, 2, 207, '1', '1', '1', '1'),
(362, 2, 208, '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `audit_logs`
--

CREATE TABLE `audit_logs` (
  `id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) DEFAULT NULL,
  `controller` varchar(225) DEFAULT NULL,
  `_action` varchar(225) DEFAULT NULL,
  `type` varchar(250) DEFAULT NULL,
  `primary_key` int(11) DEFAULT NULL,
  `source` varchar(250) DEFAULT NULL,
  `parent_source` varchar(250) DEFAULT NULL,
  `original` text,
  `changed` text,
  `meta` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `audit_logs`
--

INSERT INTO `audit_logs` (`id`, `timestamp`, `user_id`, `controller`, `_action`, `type`, `primary_key`, `source`, `parent_source`, `original`, `changed`, `meta`) VALUES
(1, '2018-02-06 15:08:55', 1, 'users', 'add', 'create', 10, 'users', NULL, '\"{\\\"id\\\":10,\\\"username\\\":\\\"sadsadsad\\\",\\\"name\\\":\\\"KAS PUSAT\\\",\\\"email\\\":\\\"sadsa23dsadsa@gmail.com\\\",\\\"group_id\\\":2,\\\"status\\\":false,\\\"created_by\\\":1}\"', '\"{\\\"id\\\":10,\\\"username\\\":\\\"sadsadsad\\\",\\\"name\\\":\\\"KAS PUSAT\\\",\\\"email\\\":\\\"sadsa23dsadsa@gmail.com\\\",\\\"group_id\\\":2,\\\"status\\\":false,\\\"created_by\\\":1}\"', '\"[]\"'),
(2, '2018-02-06 15:11:34', 1, 'groups', 'delete', 'delete', 10, 'users', NULL, 'null', 'null', '\"[]\"'),
(3, '2018-02-06 15:11:34', 1, 'groups', 'delete', 'delete', 2, 'groups', NULL, 'null', 'null', '\"[]\"'),
(4, '2018-02-08 10:38:04', 1, 'users', 'add', 'create', 2, 'users', NULL, '\"{\\\"id\\\":2,\\\"username\\\":\\\"admin\\\",\\\"name\\\":\\\"dsadsa\\\",\\\"email\\\":\\\"dsadsads@dsadsa.com\\\",\\\"group_id\\\":1,\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"{\\\"id\\\":2,\\\"username\\\":\\\"admin\\\",\\\"name\\\":\\\"dsadsa\\\",\\\"email\\\":\\\"dsadsads@dsadsa.com\\\",\\\"group_id\\\":1,\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"[]\"'),
(5, '2018-02-08 10:38:27', 1, 'users', 'add', 'create', 3, 'users', NULL, '\"{\\\"id\\\":3,\\\"username\\\":\\\"dsadsad\\\",\\\"name\\\":\\\"KAS PUSAT\\\",\\\"email\\\":\\\"321321@gmail.com\\\",\\\"group_id\\\":1,\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"{\\\"id\\\":3,\\\"username\\\":\\\"dsadsad\\\",\\\"name\\\":\\\"KAS PUSAT\\\",\\\"email\\\":\\\"321321@gmail.com\\\",\\\"group_id\\\":1,\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"[]\"'),
(6, '2018-02-09 03:50:34', 1, 'users', 'edit', 'update', 2, 'users', NULL, '\"{\\\"username\\\":\\\"admin\\\",\\\"name\\\":\\\"dsadsa\\\",\\\"email\\\":\\\"dsadsads@dsadsa.com\\\",\\\"modified_by\\\":null}\"', '\"{\\\"username\\\":\\\"rezama\\\",\\\"name\\\":\\\"rezama\\\",\\\"email\\\":\\\"rezama@app.com\\\",\\\"modified_by\\\":1}\"', '\"[]\"'),
(7, '2018-02-09 04:14:55', 1, 'groups', 'add', 'create', 2, 'groups', NULL, '\"{\\\"id\\\":2,\\\"name\\\":\\\"SPV\\\",\\\"status\\\":false,\\\"created_by\\\":1}\"', '\"{\\\"id\\\":2,\\\"name\\\":\\\"SPV\\\",\\\"status\\\":false,\\\"created_by\\\":1}\"', '\"[]\"'),
(8, '2018-02-09 04:15:23', 1, 'users', 'add', 'create', 4, 'users', NULL, '\"{\\\"id\\\":4,\\\"username\\\":\\\"SPV\\\",\\\"name\\\":\\\"spv\\\",\\\"email\\\":\\\"spv@app.com\\\",\\\"group_id\\\":1,\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"{\\\"id\\\":4,\\\"username\\\":\\\"SPV\\\",\\\"name\\\":\\\"spv\\\",\\\"email\\\":\\\"spv@app.com\\\",\\\"group_id\\\":1,\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"[]\"'),
(9, '2018-02-09 04:15:32', 1, 'users', 'edit', 'update', 2, 'users', NULL, '\"{\\\"status\\\":true,\\\"modified_by\\\":1}\"', '\"{\\\"status\\\":false,\\\"modified_by\\\":1}\"', '\"[]\"'),
(10, '2018-03-01 16:49:08', 1, 'edit', '2', 'update', 2, 'users', NULL, '\"{\\\"status\\\":false,\\\"modified_by\\\":1}\"', '\"{\\\"status\\\":true,\\\"modified_by\\\":1}\"', '\"[]\"'),
(11, '2018-03-02 07:10:57', 1, 'edit', '2', 'update', 2, 'users', NULL, '\"{\\\"name\\\":\\\"rezama\\\",\\\"modified_by\\\":1}\"', '\"{\\\"name\\\":\\\"rezama1\\\",\\\"modified_by\\\":1}\"', '\"[]\"'),
(12, '2018-03-02 07:19:29', 2, 'edit', '2', 'update', 2, 'users', NULL, '\"{\\\"name\\\":\\\"rezama1\\\",\\\"modified_by\\\":1}\"', '\"{\\\"name\\\":\\\"rezama123445\\\",\\\"modified_by\\\":2}\"', '\"[]\"'),
(13, '2018-03-02 07:20:27', 1, 'edit', '2', 'update', 2, 'users', NULL, '\"{\\\"email\\\":\\\"rezama@app.com\\\",\\\"modified_by\\\":2}\"', '\"{\\\"email\\\":\\\"rezam3213a@app.com\\\",\\\"modified_by\\\":1}\"', '\"[]\"'),
(14, '2018-03-02 07:24:39', 1, 'edit', '2', 'update', 2, 'users', NULL, '\"{\\\"username\\\":\\\"rezama\\\",\\\"name\\\":\\\"rezama123445\\\",\\\"modified_by\\\":1}\"', '\"{\\\"username\\\":\\\"rezama123\\\",\\\"name\\\":\\\"udinpetot\\\",\\\"modified_by\\\":1}\"', '\"[]\"'),
(15, '2018-03-02 07:28:28', 1, 'edit', '2', 'update', 2, 'users', NULL, '\"{\\\"username\\\":\\\"rezama123\\\",\\\"name\\\":\\\"udinpetot\\\",\\\"modified_by\\\":1}\"', '\"{\\\"username\\\":\\\"rezama\\\",\\\"name\\\":\\\"rezama ryan\\\",\\\"modified_by\\\":1}\"', '\"[]\"'),
(16, '2018-03-02 07:33:55', 1, 'edit', '2', 'update', 2, 'users', NULL, '\"{\\\"group_id\\\":1,\\\"modified_by\\\":1}\"', '\"{\\\"group_id\\\":2,\\\"modified_by\\\":1}\"', '\"[]\"'),
(17, '2018-03-02 17:00:55', 1, 'delete', '3', 'delete', 3, 'users', NULL, 'null', 'null', '\"[]\"'),
(18, '2018-06-04 16:33:47', 1, 'edit', '2', 'update', 2, 'groups', NULL, '\"{\\\"name\\\":\\\"SPV\\\",\\\"status\\\":false,\\\"modified_by\\\":null}\"', '\"{\\\"name\\\":\\\"DEVELOPER\\\",\\\"status\\\":true,\\\"modified_by\\\":1}\"', '\"[]\"'),
(19, '2018-06-04 17:39:34', 1, 'add', NULL, 'create', 3, 'groups', NULL, '\"{\\\"id\\\":3,\\\"name\\\":\\\"DPP\\\",\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"{\\\"id\\\":3,\\\"name\\\":\\\"DPP\\\",\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"[]\"'),
(20, '2018-06-04 17:39:44', 1, 'add', NULL, 'create', 4, 'groups', NULL, '\"{\\\"id\\\":4,\\\"name\\\":\\\"DPD\\\",\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"{\\\"id\\\":4,\\\"name\\\":\\\"DPD\\\",\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"[]\"'),
(21, '2018-06-04 17:39:58', 1, 'add', NULL, 'create', 5, 'groups', NULL, '\"{\\\"id\\\":5,\\\"name\\\":\\\"KONSULTANT\\\",\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"{\\\"id\\\":5,\\\"name\\\":\\\"KONSULTANT\\\",\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"[]\"'),
(22, '2018-06-05 16:35:51', 9, NULL, NULL, 'update', 9, 'users', NULL, '\"{\\\"name\\\":\\\"aiqbalsyah@gmail.com\\\",\\\"modified_by\\\":null}\"', '\"{\\\"name\\\":\\\"Ardiansyah Iqbal\\\",\\\"modified_by\\\":9}\"', '\"[]\"'),
(23, '2018-06-05 16:39:32', 10, NULL, NULL, 'update', 10, 'users', NULL, '\"{\\\"modified_by\\\":1}\"', '\"{\\\"modified_by\\\":10}\"', '\"[]\"'),
(24, '2018-06-05 16:44:02', 8, NULL, NULL, 'update', 8, 'users', NULL, '\"{\\\"modified_by\\\":1}\"', '\"{\\\"modified_by\\\":8}\"', '\"[]\"'),
(25, '2018-06-05 16:48:31', 8, NULL, NULL, 'update', 8, 'users', NULL, '\"{\\\"modified_by\\\":1}\"', '\"{\\\"modified_by\\\":8}\"', '\"[]\"'),
(26, '2018-06-16 15:45:38', 10, 'delete', '2', 'delete', 6, 'users', NULL, NULL, NULL, '[]'),
(27, '2018-06-16 15:45:38', 10, 'delete', '2', 'delete', 2, 'developers', NULL, NULL, NULL, '[]'),
(28, '2018-06-16 15:47:15', NULL, NULL, NULL, 'create', 11, 'users', NULL, '{\"id\":null,\"username\":\"testdev\",\"name\":\"TESTDEV\",\"email\":\"test@gmail.com\",\"group_id\":2,\"status\":false}', '{\"id\":11,\"username\":\"testdev\",\"name\":\"TESTDEV\",\"email\":\"test@gmail.com\",\"group_id\":2,\"status\":false}', '[]'),
(29, '2018-06-16 15:47:15', NULL, NULL, NULL, 'create', 4, 'developers', NULL, '{\"id\":4,\"name\":\"TESTDEV\",\"ceo_name\":\"UDIN\",\"kta_number\":\"100239101\",\"dpp_from_id\":17,\"address\":\"TEST\",\"phone\":\"0239102,3201293210\",\"email\":\"test@gmail.com\"}', '{\"id\":4,\"name\":\"TESTDEV\",\"ceo_name\":\"UDIN\",\"kta_number\":\"100239101\",\"dpp_from_id\":17,\"address\":\"TEST\",\"phone\":\"0239102,3201293210\",\"email\":\"test@gmail.com\"}', '[]'),
(30, '2018-06-16 15:48:49', NULL, NULL, NULL, 'create', 12, 'users', NULL, '{\"id\":12,\"username\":\"testdev2\",\"name\":\"testdev\",\"email\":\"3213@gmail.com\",\"group_id\":2,\"status\":false}', '{\"id\":12,\"username\":\"testdev2\",\"name\":\"testdev\",\"email\":\"3213@gmail.com\",\"group_id\":2,\"status\":false}', '[]'),
(31, '2018-06-16 15:48:49', NULL, NULL, NULL, 'create', 5, 'developers', NULL, '{\"id\":5,\"name\":\"testdev\",\"ceo_name\":\"testdsadsa\",\"kta_number\":\"213213\",\"dpp_from_id\":51,\"address\":\"21312321\",\"phone\":\"32193201\",\"email\":\"3213@gmail.com\"}', '{\"id\":5,\"name\":\"testdev\",\"ceo_name\":\"testdsadsa\",\"kta_number\":\"213213\",\"dpp_from_id\":51,\"address\":\"21312321\",\"phone\":\"32193201\",\"email\":\"3213@gmail.com\"}', '[]'),
(32, '2018-06-16 15:51:49', NULL, NULL, NULL, 'create', 6, 'users', NULL, '{\"id\":6,\"username\":\"testdefv3\",\"name\":\"saaskj\",\"email\":\"teffj@gmail.com\",\"group_id\":2,\"status\":false}', '{\"id\":6,\"username\":\"testdefv3\",\"name\":\"saaskj\",\"email\":\"teffj@gmail.com\",\"group_id\":2,\"status\":false}', '[]'),
(33, '2018-06-16 15:51:49', NULL, NULL, NULL, 'create', 6, 'developers', NULL, '{\"id\":6,\"name\":\"saaskj\",\"ceo_name\":\"kadjlak\",\"kta_number\":\"20190121\",\"dpp_from_id\":36,\"address\":\"oiueiow\",\"phone\":\"021901290\",\"email\":\"teffj@gmail.com\"}', '{\"id\":6,\"name\":\"saaskj\",\"ceo_name\":\"kadjlak\",\"kta_number\":\"20190121\",\"dpp_from_id\":36,\"address\":\"oiueiow\",\"phone\":\"021901290\",\"email\":\"teffj@gmail.com\"}', '[]'),
(34, '2018-06-16 15:54:25', NULL, NULL, NULL, 'create', 13, 'users', NULL, '{\"id\":13,\"username\":\"indoy\",\"name\":\"indoy developer\",\"email\":\"indoy@gmail.com\",\"group_id\":2,\"status\":false}', '{\"id\":13,\"username\":\"indoy\",\"name\":\"indoy developer\",\"email\":\"indoy@gmail.com\",\"group_id\":2,\"status\":false}', '[]'),
(35, '2018-06-16 15:54:25', NULL, NULL, NULL, 'create', 7, 'developers', NULL, '{\"id\":7,\"name\":\"indoy developer\",\"ceo_name\":\"INDOY ASPIRIN\",\"kta_number\":\"100032191\",\"dpp_from_id\":32,\"address\":\"CIDERUM CITY\",\"phone\":\"0923123921\",\"email\":\"indoy@gmail.com\"}', '{\"id\":7,\"name\":\"indoy developer\",\"ceo_name\":\"INDOY ASPIRIN\",\"kta_number\":\"100032191\",\"dpp_from_id\":32,\"address\":\"CIDERUM CITY\",\"phone\":\"0923123921\",\"email\":\"indoy@gmail.com\"}', '[]'),
(36, '2018-06-16 15:58:23', NULL, NULL, NULL, 'create', 14, 'users', NULL, '{\"id\":14,\"username\":\"aiqbalsyah23\",\"name\":\"iqbal ganteng\",\"email\":\"aiqbalsyahds@gmail.com\",\"group_id\":2,\"status\":false}', '{\"id\":14,\"username\":\"aiqbalsyah23\",\"name\":\"iqbal ganteng\",\"email\":\"aiqbalsyahds@gmail.com\",\"group_id\":2,\"status\":false}', '[]'),
(37, '2018-06-16 15:58:23', NULL, NULL, NULL, 'create', 8, 'developers', NULL, '{\"id\":8,\"name\":\"iqbal ganteng\",\"ceo_name\":\"iqbal\",\"kta_number\":\"100203190\",\"dpp_from_id\":32,\"address\":\"tur\",\"phone\":\"29302139201\",\"email\":\"aiqbalsyahds@gmail.com\"}', '{\"id\":8,\"name\":\"iqbal ganteng\",\"ceo_name\":\"iqbal\",\"kta_number\":\"100203190\",\"dpp_from_id\":32,\"address\":\"tur\",\"phone\":\"29302139201\",\"email\":\"aiqbalsyahds@gmail.com\"}', '[]'),
(38, '2018-06-16 15:59:21', NULL, NULL, NULL, 'create', 15, 'users', NULL, '{\"id\":15,\"username\":\"aiqbalsyah2309\",\"name\":\"indoy\",\"email\":\"sdskjd@gmail.com\",\"group_id\":2,\"status\":false}', '{\"id\":15,\"username\":\"aiqbalsyah2309\",\"name\":\"indoy\",\"email\":\"sdskjd@gmail.com\",\"group_id\":2,\"status\":false}', '[]'),
(39, '2018-06-16 15:59:21', NULL, NULL, NULL, 'create', 9, 'developers', NULL, '{\"id\":9,\"name\":\"indoy\",\"ceo_name\":\"21323901\",\"kta_number\":\"392103920\",\"dpp_from_id\":15,\"address\":\"238123192\",\"phone\":\"219302190\",\"email\":\"sdskjd@gmail.com\",\"user_id\":15}', '{\"id\":9,\"name\":\"indoy\",\"ceo_name\":\"21323901\",\"kta_number\":\"392103920\",\"dpp_from_id\":15,\"address\":\"238123192\",\"phone\":\"219302190\",\"email\":\"sdskjd@gmail.com\",\"user_id\":15}', '[]'),
(40, '2018-06-16 16:00:26', 10, 'delete', '8', 'delete', 8, 'developers', NULL, NULL, NULL, '[]'),
(41, '2018-06-16 16:00:30', 10, 'delete', '3', 'delete', 3, 'developers', NULL, NULL, NULL, '[]'),
(42, '2018-06-16 16:01:54', 10, 'delete', '9', 'delete', 9, 'developers', NULL, NULL, NULL, '[]'),
(43, '2018-06-16 16:02:44', NULL, NULL, NULL, 'create', 16, 'users', NULL, '{\"id\":16,\"username\":\"asdfasf\",\"name\":\"test\",\"email\":\"sadas@gs.com\",\"group_id\":2,\"status\":false}', '{\"id\":16,\"username\":\"asdfasf\",\"name\":\"test\",\"email\":\"sadas@gs.com\",\"group_id\":2,\"status\":false}', '[]'),
(44, '2018-06-16 16:02:44', NULL, NULL, NULL, 'create', 10, 'developers', NULL, '{\"id\":10,\"name\":\"test\",\"ceo_name\":\"test\",\"kta_number\":\"test\",\"dpp_from_id\":51,\"address\":\"sklajdsalkd\",\"phone\":\"239012390\",\"email\":\"sadas@gs.com\",\"user_id\":16}', '{\"id\":10,\"name\":\"test\",\"ceo_name\":\"test\",\"kta_number\":\"test\",\"dpp_from_id\":51,\"address\":\"sklajdsalkd\",\"phone\":\"239012390\",\"email\":\"sadas@gs.com\",\"user_id\":16}', '[]'),
(45, '2018-06-16 16:03:56', NULL, NULL, NULL, 'create', 17, 'users', NULL, '{\"id\":17,\"username\":\"aiqbalsyah\",\"name\":\"aku terganteng\",\"email\":\"indo@gma.com\",\"group_id\":2,\"status\":false}', '{\"id\":17,\"username\":\"aiqbalsyah\",\"name\":\"aku terganteng\",\"email\":\"indo@gma.com\",\"group_id\":2,\"status\":false}', '[]'),
(46, '2018-06-16 16:03:56', NULL, NULL, NULL, 'create', 11, 'developers', NULL, '{\"id\":11,\"name\":\"aku terganteng\",\"ceo_name\":\"iqbal\",\"kta_number\":\"2390139201\",\"dpp_from_id\":36,\"address\":\"preeet\",\"phone\":\"12039210\",\"email\":\"indo@gma.com\",\"user_id\":17}', '{\"id\":11,\"name\":\"aku terganteng\",\"ceo_name\":\"iqbal\",\"kta_number\":\"2390139201\",\"dpp_from_id\":36,\"address\":\"preeet\",\"phone\":\"12039210\",\"email\":\"indo@gma.com\",\"user_id\":17}', '[]'),
(47, '2018-06-16 16:04:42', 10, 'delete', '11', 'delete', 11, 'developers', NULL, NULL, NULL, '[]'),
(48, '2018-06-16 16:08:09', 10, 'delete', '10', 'delete', 10, 'developers', NULL, NULL, NULL, '[]'),
(49, '2018-06-16 16:23:46', 1, 'add', NULL, 'create', 18, 'users', NULL, '{\"id\":18,\"username\":\"konsultan\",\"name\":\"konsultant\",\"email\":\"konsultan@gmail.com\",\"group_id\":5,\"status\":true,\"created_by\":1}', '{\"id\":18,\"username\":\"konsultan\",\"name\":\"konsultant\",\"email\":\"konsultan@gmail.com\",\"group_id\":5,\"status\":true,\"created_by\":1}', '[]'),
(50, '2018-06-16 16:41:21', NULL, NULL, NULL, 'create', 19, 'users', NULL, '{\"id\":19,\"username\":\"india\",\"name\":\"indoy corporation\",\"email\":\"indi@gmail.com\",\"group_id\":2,\"status\":false}', '{\"id\":19,\"username\":\"india\",\"name\":\"indoy corporation\",\"email\":\"indi@gmail.com\",\"group_id\":2,\"status\":false}', '[]'),
(51, '2018-06-16 16:41:21', NULL, NULL, NULL, 'create', 12, 'developers', NULL, '{\"id\":12,\"name\":\"indoy corporation\",\"ceo_name\":\"Indi Aspiani\",\"kta_number\":\"1000321901\",\"dpp_from_id\":32,\"address\":\"Check Baby Check One Two Three\",\"phone\":\"08583291328\",\"email\":\"indi@gmail.com\",\"user_id\":19}', '{\"id\":12,\"name\":\"indoy corporation\",\"ceo_name\":\"Indi Aspiani\",\"kta_number\":\"1000321901\",\"dpp_from_id\":32,\"address\":\"Check Baby Check One Two Three\",\"phone\":\"08583291328\",\"email\":\"indi@gmail.com\",\"user_id\":19}', '[]'),
(52, '2018-06-16 17:06:56', NULL, NULL, NULL, 'create', 20, 'users', NULL, '{\"id\":20,\"username\":\"sdjsal\",\"name\":\"test\",\"email\":\"aiqbwe@dasd.com\",\"group_id\":2,\"status\":false}', '{\"id\":20,\"username\":\"sdjsal\",\"name\":\"test\",\"email\":\"aiqbwe@dasd.com\",\"group_id\":2,\"status\":false}', '[]'),
(53, '2018-06-16 17:06:56', NULL, NULL, NULL, 'create', 13, 'developers', NULL, '{\"id\":13,\"name\":\"test\",\"ceo_name\":\"test\",\"kta_number\":\"test\",\"dpp_from_id\":51,\"address\":\"Jsdsakj\",\"phone\":\"sldkjadkls\",\"email\":\"aiqbwe@dasd.com\",\"user_id\":20}', '{\"id\":13,\"name\":\"test\",\"ceo_name\":\"test\",\"kta_number\":\"test\",\"dpp_from_id\":51,\"address\":\"Jsdsakj\",\"phone\":\"sldkjadkls\",\"email\":\"aiqbwe@dasd.com\",\"user_id\":20}', '[]'),
(54, '2018-06-16 17:07:15', NULL, NULL, NULL, 'create', 21, 'users', NULL, '{\"id\":21,\"username\":\"21321\",\"name\":\"dsadsa\",\"email\":\"saddas@fasd.com\",\"group_id\":2,\"status\":false}', '{\"id\":21,\"username\":\"21321\",\"name\":\"dsadsa\",\"email\":\"saddas@fasd.com\",\"group_id\":2,\"status\":false}', '[]'),
(55, '2018-06-16 17:07:15', NULL, NULL, NULL, 'create', 14, 'developers', NULL, '{\"id\":14,\"name\":\"dsadsa\",\"ceo_name\":\"sadad\",\"kta_number\":\"sadas\",\"dpp_from_id\":32,\"address\":\"sdasd\",\"phone\":\"13213\",\"email\":\"saddas@fasd.com\",\"user_id\":21}', '{\"id\":14,\"name\":\"dsadsa\",\"ceo_name\":\"sadad\",\"kta_number\":\"sadas\",\"dpp_from_id\":32,\"address\":\"sdasd\",\"phone\":\"13213\",\"email\":\"saddas@fasd.com\",\"user_id\":21}', '[]'),
(56, '2018-06-16 17:10:35', NULL, NULL, NULL, 'create', 22, 'users', NULL, '{\"id\":22,\"username\":\"1239210\",\"name\":\"dsadsadsa\",\"email\":\"das@fmail.com\",\"group_id\":2,\"status\":false}', '{\"id\":22,\"username\":\"1239210\",\"name\":\"dsadsadsa\",\"email\":\"das@fmail.com\",\"group_id\":2,\"status\":false}', '[]'),
(57, '2018-06-16 17:10:35', NULL, NULL, NULL, 'create', 15, 'developers', NULL, '{\"id\":15,\"name\":\"dsadsadsa\",\"ceo_name\":\"dsdsadsa\",\"kta_number\":\"sadsadsa\",\"dpp_from_id\":51,\"address\":\"sadasd\",\"phone\":\"213910\",\"email\":\"das@fmail.com\",\"pic\":\"PJ\",\"pic_phone\":\"293012PJ\",\"user_id\":22}', '{\"id\":15,\"name\":\"dsadsadsa\",\"ceo_name\":\"dsdsadsa\",\"kta_number\":\"sadsadsa\",\"dpp_from_id\":51,\"address\":\"sadasd\",\"phone\":\"213910\",\"email\":\"das@fmail.com\",\"pic\":\"PJ\",\"pic_phone\":\"293012PJ\",\"user_id\":22}', '[]'),
(58, '2018-06-16 17:11:58', NULL, NULL, NULL, 'create', 23, 'users', NULL, '{\"id\":23,\"username\":\"23901239102\",\"name\":\"dsadsadsad\",\"email\":\"dsada@gmai.com\",\"group_id\":2,\"status\":false}', '{\"id\":23,\"username\":\"23901239102\",\"name\":\"dsadsadsad\",\"email\":\"dsada@gmai.com\",\"group_id\":2,\"status\":false}', '[]'),
(59, '2018-06-16 17:11:58', NULL, NULL, NULL, 'create', 16, 'developers', NULL, '{\"id\":16,\"name\":\"dsadsadsad\",\"ceo_name\":\"dsadasds\",\"kta_number\":\"dsadsadsa\",\"dpp_from_id\":35,\"address\":\"dsadsadsa\",\"phone\":\"s9023190\",\"email\":\"dsada@gmai.com\",\"pic\":\"392103\",\"pic_phone\":\"3912039201\",\"user_id\":23}', '{\"id\":16,\"name\":\"dsadsadsad\",\"ceo_name\":\"dsadasds\",\"kta_number\":\"dsadsadsa\",\"dpp_from_id\":35,\"address\":\"dsadsadsa\",\"phone\":\"s9023190\",\"email\":\"dsada@gmai.com\",\"pic\":\"392103\",\"pic_phone\":\"3912039201\",\"user_id\":23}', '[]'),
(60, '2018-06-16 17:12:23', NULL, NULL, NULL, 'create', 24, 'users', NULL, '{\"id\":24,\"username\":\"21938921398\",\"name\":\"dsadsadsa\",\"email\":\"3213@dasd.com\",\"group_id\":2,\"status\":false}', '{\"id\":24,\"username\":\"21938921398\",\"name\":\"dsadsadsa\",\"email\":\"3213@dasd.com\",\"group_id\":2,\"status\":false}', '[]'),
(61, '2018-06-16 17:12:23', NULL, NULL, NULL, 'create', 17, 'developers', NULL, '{\"id\":17,\"name\":\"dsadsadsa\",\"ceo_name\":\"dsadsad\",\"kta_number\":\"dsadsa\",\"dpp_from_id\":34,\"address\":\"321321\",\"phone\":\"21312839321\",\"email\":\"3213@dasd.com\",\"pic\":\"21938129389\",\"pic_phone\":\"2381938192\",\"user_id\":24}', '{\"id\":17,\"name\":\"dsadsadsa\",\"ceo_name\":\"dsadsad\",\"kta_number\":\"dsadsa\",\"dpp_from_id\":34,\"address\":\"321321\",\"phone\":\"21312839321\",\"email\":\"3213@dasd.com\",\"pic\":\"21938129389\",\"pic_phone\":\"2381938192\",\"user_id\":24}', '[]'),
(62, '2018-06-16 17:13:27', NULL, NULL, NULL, 'create', 25, 'users', NULL, '{\"id\":25,\"username\":\"2381293892\",\"name\":\"dsadsadsaddsa\",\"email\":\"asdf@gmail.com\",\"group_id\":2,\"status\":false}', '{\"id\":25,\"username\":\"2381293892\",\"name\":\"dsadsadsaddsa\",\"email\":\"asdf@gmail.com\",\"group_id\":2,\"status\":false}', '[]'),
(63, '2018-06-16 17:13:27', NULL, NULL, NULL, 'create', 18, 'developers', NULL, '{\"id\":18,\"name\":\"dsadsadsaddsa\",\"ceo_name\":\"dasdsadas\",\"kta_number\":\"dsad\",\"dpp_from_id\":36,\"address\":\"321312\",\"phone\":\"23139\",\"email\":\"asdf@gmail.com\",\"pic\":\"2382198\",\"pic_phone\":\"321931289\",\"user_id\":25}', '{\"id\":18,\"name\":\"dsadsadsaddsa\",\"ceo_name\":\"dasdsadas\",\"kta_number\":\"dsad\",\"dpp_from_id\":36,\"address\":\"321312\",\"phone\":\"23139\",\"email\":\"asdf@gmail.com\",\"pic\":\"2382198\",\"pic_phone\":\"321931289\",\"user_id\":25}', '[]'),
(64, '2018-06-16 17:14:08', 10, 'edit', '18', 'update', 18, 'developers', NULL, '{\"user_id\":25,\"status\":false,\"modified_by\":null}', '{\"user_id\":25,\"status\":true,\"modified_by\":10}', '[]'),
(65, '2018-06-16 17:14:50', 10, 'edit', '15', 'update', 15, 'developers', NULL, '{\"user_id\":22,\"status\":false,\"modified_by\":null}', '{\"user_id\":22,\"status\":true,\"modified_by\":10}', '[]'),
(66, '2018-06-16 17:15:00', 10, 'edit', '13', 'update', 13, 'developers', NULL, '{\"user_id\":20,\"status\":false,\"modified_by\":null}', '{\"user_id\":20,\"status\":true,\"modified_by\":10}', '[]'),
(67, '2018-06-16 17:15:11', 10, 'delete', '13', 'delete', 13, 'developers', NULL, NULL, NULL, '[]'),
(68, '2018-06-16 17:15:15', 10, 'delete', '15', 'delete', 15, 'developers', NULL, NULL, NULL, '[]'),
(69, '2018-06-16 17:15:20', 10, 'delete', '18', 'delete', 18, 'developers', NULL, NULL, NULL, '[]'),
(70, '2018-06-16 17:15:24', 10, 'delete', '12', 'delete', 12, 'developers', NULL, NULL, NULL, '[]'),
(71, '2018-06-16 17:15:29', 10, 'delete', '14', 'delete', 14, 'developers', NULL, NULL, NULL, '[]'),
(72, '2018-06-16 17:15:34', 10, 'delete', '16', 'delete', 16, 'developers', NULL, NULL, NULL, '[]'),
(73, '2018-06-16 17:15:38', 10, 'delete', '17', 'delete', 17, 'developers', NULL, NULL, NULL, '[]'),
(74, '2018-06-16 20:12:33', NULL, NULL, NULL, 'create', 26, 'users', NULL, '{\"id\":26,\"username\":\"indi\",\"name\":\"PT. INDI HOME\",\"email\":\"indi.home@gmail.com\",\"group_id\":2,\"status\":false}', '{\"id\":26,\"username\":\"indi\",\"name\":\"PT. INDI HOME\",\"email\":\"indi.home@gmail.com\",\"group_id\":2,\"status\":false}', '[]'),
(75, '2018-06-16 20:12:33', NULL, NULL, NULL, 'create', 19, 'developers', NULL, '{\"id\":19,\"name\":\"PT. INDI HOME\",\"ceo_name\":\"Indi Aspiani\",\"kta_number\":\"10000001\",\"dpp_from_id\":32,\"address\":\"Ciderum\",\"phone\":\"08583212391\",\"email\":\"indi.home@gmail.com\",\"pic\":\"Ardiansyah\",\"pic_phone\":\"32932103219,3219321032\",\"user_id\":26}', '{\"id\":19,\"name\":\"PT. INDI HOME\",\"ceo_name\":\"Indi Aspiani\",\"kta_number\":\"10000001\",\"dpp_from_id\":32,\"address\":\"Ciderum\",\"phone\":\"08583212391\",\"email\":\"indi.home@gmail.com\",\"pic\":\"Ardiansyah\",\"pic_phone\":\"32932103219,3219321032\",\"user_id\":26}', '[]'),
(76, '2018-06-16 20:13:35', 10, 'edit', '19', 'update', 26, 'users', NULL, '{\"status\":false,\"modified_by\":null}', '{\"status\":true,\"modified_by\":10}', '[]'),
(77, '2018-06-16 20:13:35', 10, 'edit', '19', 'update', 19, 'developers', NULL, '{\"user_id\":26,\"status\":false,\"modified_by\":null}', '{\"user_id\":26,\"status\":true,\"modified_by\":10}', '[]'),
(78, '2018-07-15 14:25:40', 1, 'delete', '5', 'delete', 18, 'users', NULL, NULL, NULL, '[]'),
(79, '2018-07-15 14:25:40', 1, 'delete', '5', 'delete', 5, 'groups', NULL, NULL, NULL, '[]'),
(80, '2018-07-15 14:25:44', 1, 'delete', '4', 'delete', 7, 'users', NULL, NULL, NULL, '[]'),
(81, '2018-07-15 14:25:44', 1, 'delete', '4', 'delete', 8, 'users', NULL, NULL, NULL, '[]'),
(82, '2018-07-15 14:25:44', 1, 'delete', '4', 'delete', 4, 'groups', NULL, NULL, NULL, '[]'),
(83, '2018-07-15 14:25:47', 1, 'delete', '3', 'delete', 10, 'users', NULL, NULL, NULL, '[]'),
(84, '2018-07-15 14:25:47', 1, 'delete', '3', 'delete', 3, 'groups', NULL, NULL, NULL, '[]'),
(85, '2018-07-15 14:25:52', 1, 'delete', '2', 'delete', 2, 'users', NULL, NULL, NULL, '[]'),
(86, '2018-07-15 14:25:52', 1, 'delete', '2', 'delete', 26, 'users', NULL, NULL, NULL, '[]'),
(87, '2018-07-15 14:25:52', 1, 'delete', '2', 'delete', 2, 'groups', NULL, NULL, NULL, '[]'),
(88, '2018-07-15 14:37:19', 1, 'delete', '4', 'delete', 4, 'users', NULL, NULL, NULL, '[]'),
(89, '2018-07-25 13:25:45', 1, 'add', NULL, 'create', 2, 'groups', NULL, '{\"id\":2,\"name\":\"tot\",\"status\":false,\"created_by\":1}', '{\"id\":2,\"name\":\"tot\",\"status\":false,\"created_by\":1}', '[]'),
(90, '2018-07-25 13:38:46', 1, 'edit', '1', 'update', 1, 'groups', NULL, '{\"name\":\"administrator\",\"modified_by\":null}', '{\"name\":\"administrator1\",\"modified_by\":1}', '[]'),
(91, '2018-07-25 13:39:33', 1, 'edit', '1', 'update', 1, 'groups', NULL, '{\"name\":\"administrator1\",\"modified_by\":1}', '{\"name\":\"administrator\",\"modified_by\":1}', '[]'),
(92, '2018-07-25 15:00:13', 1, 'add', NULL, 'create', 3, 'groups', NULL, '{\"id\":3,\"name\":\"asdf\",\"status\":false,\"created_by\":1}', '{\"id\":3,\"name\":\"asdf\",\"status\":false,\"created_by\":1}', '[]'),
(93, '2018-07-25 15:02:02', 1, 'delete', '3', 'delete', 3, 'groups', NULL, NULL, NULL, '[]'),
(94, '2018-07-25 15:02:06', 1, 'delete', '2', 'delete', 2, 'groups', NULL, NULL, NULL, '[]'),
(95, '2018-07-25 20:10:12', 1, 'add', NULL, 'create', 4, 'groups', NULL, '{\"id\":4,\"name\":\"iqbal ardiansyah\",\"status\":true,\"created_by\":1}', '{\"id\":4,\"name\":\"iqbal ardiansyah\",\"status\":true,\"created_by\":1}', '[]');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `status`, `created_by`, `created`, `modified_by`, `modified`) VALUES
(1, 'administrator', 1, NULL, '2018-02-05 08:03:19', 1, '2018-07-25 13:39:33'),
(4, 'iqbal ardiansyah', 1, 1, '2018-07-25 20:10:12', NULL, '2018-07-25 20:10:12');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` char(60) NOT NULL,
  `name` varchar(225) DEFAULT NULL,
  `email` varchar(225) DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `email`, `group_id`, `status`, `created_by`, `created`, `modified_by`, `modified`) VALUES
(1, 'administrator', '$2y$10$FFIjh73z6y6Yq09cwqWAMuzwZ3eKp8XJ3jwU1ZKlR4Rg0BtV5eogy', 'administrator', 'administrator@app.com', 1, 1, 1, '2018-02-05 08:44:27', 1, '2018-02-06 11:06:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acl_phinxlog`
--
ALTER TABLE `acl_phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `acos`
--
ALTER TABLE `acos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lft` (`lft`,`rght`),
  ADD KEY `alias` (`alias`);

--
-- Indexes for table `app_settings`
--
ALTER TABLE `app_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aros`
--
ALTER TABLE `aros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lft` (`lft`,`rght`),
  ADD KEY `alias` (`alias`);

--
-- Indexes for table `aros_acos`
--
ALTER TABLE `aros_acos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `aro_id` (`aro_id`,`aco_id`),
  ADD KEY `aco_id` (`aco_id`);

--
-- Indexes for table `audit_logs`
--
ALTER TABLE `audit_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `username` (`username`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acos`
--
ALTER TABLE `acos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=209;

--
-- AUTO_INCREMENT for table `app_settings`
--
ALTER TABLE `app_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `aros`
--
ALTER TABLE `aros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `aros_acos`
--
ALTER TABLE `aros_acos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=363;

--
-- AUTO_INCREMENT for table `audit_logs`
--
ALTER TABLE `audit_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
