<?php
namespace App\Listener;
use Cake\Event\EventListenerInterface;
use Cake\Event\Event;
use Cake\Network\Request;

class BeforeSaveListener  implements EventListenerInterface
{

    public function __construct()
    {
        // $controller = $this->_registry->getController();
        // $this->setEventManager($controller->getEventManager());
        // $this->response =& $controller->response;
        
    }
    

    public function implementedEvents()
    {
        //pr($this);
        return [
            'Model.beforeSave' => 'beforeSave',
        ];
    }

    public function beforeSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, 
    \ArrayObject $options)
    {
        $session = new Request();
        $authId = $session->session()->read('Auth.User.id');
        $table = $event->getSubject();
        $columns = $table->schema()->columns();
        if(in_array('modified_by',$columns) && $entity->isNew() == false){
            // /pr($auth->user('id'));
            if(!empty($authId)){
                $entity->modified_by = $authId;
            }
        }
        if(in_array('created_by',$columns) && $entity->isNew() == true){
            // /pr($auth->user('id'));
            if(!empty($authId)){
                $entity->created_by = $authId;
            }
        }
        return true;
    }
}