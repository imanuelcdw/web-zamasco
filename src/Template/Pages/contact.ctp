
<div class="owl-carousel owl-theme" id="banner">
<?php foreach($banners as $banner): ?>
  <div class="page-title " style="background-image: url(<?= substr($banner->picture_dir,7).$banner->picture ?>); background-position: center;">
    <div class="container">
      <h1><?= ucfirst(strtolower($banner->pages)) ?></h1>
      <ul>
        <li><a href="<?= $this->url->build(['action'=>'index']) ?>">Home</a></li>
        <li><a href="<?= $this->url->build(['action'=>strtolower($banner->pages)]) ?>"><?= ucfirst(strtolower($banner->pages)) ?></a></li>

      </ul>
    </div>
  </div>
<?php endforeach; ?>
</div>

  <div class="section-block">
    <div class="form-group">
      <div class="container">
          <div class="row">
             <div class="col-md-6 col-sm-12">
                 <div class="section-heading" style="margin-bottom: 20px;">
                  <h2 style="text-align: center;">Format Kontak</h2>
                </div>

                <?= $this->Form->create() ?>
                    <div class="form-group">
                      <div class="col-md-12">
                        <?=$this->Form->input('name',[
                               'required' => true
                           ]); ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <?=$this->Form->input('email',[
                               'required' => true
                           ]); ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <?=$this->Form->input('password',[
                               'required' => true, 'type'=>'password'
                           ]); ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <?=$this->Form->input('subjek',[
                               'required' => true
                           ]); ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12">
                        <?=$this->Form->input('pesan',[
                               'required' => true
                           ]); ?>
                      </div>
                    </div>
                     <div class="col-md-12">
                       <button type="submit" class="btn btn-primary"><i style="font-size: 15px;" class="fa fa-envelope"></i>&nbsp;Kirim</button>
                     </div>

                <?= $this->Form->end(); ?>
              </div>
               <div class="col-md-6 col-sm-12">

                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3579.365127199295!2d106.87229081454791!3d-6.165223462137333!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f5057a82f681:0xcf8ecef827946263!2sPT+Zamasco+Mitra+Solusindo!5e1!3m2!1sen!2sid!4v1535683836038" width="600" height="435" frameborder="0" style="border:0" allowfullscreen></iframe>

                <br><br>
                <a href="https://maps.google.com?saddr=Current+Location&amp;daddr=PT+Zamasco+Mitra+Solusindo" class="btn btn-primary" target="_blank">
                <i class="fa fa-location-arrow"></i>&nbsp; Petunjuk Arah</a>&nbsp;

               </div>
          </div>
      </div>
    </div>
  </div>

  <div class="section-block-grey">
    <div class="container">
      <div class="row">

        <div class="col-md-4">
          <div class="section-heading-left">
            <h2>Kontak</h2>
          </div>
          <?php foreach ($alamat as $alamat) : ?>

          <div class="col-md-2">
            <i style="font-size: 20px;" class="fas fa-map-marker-alt"></i>
          </div>
          <div class="col-md-10">
            <p><?= $alamat->text_content; ?></p>
          </div>

          <?php endforeach; ?>
        </div>
<div class="col-md-4">
          <br><br><br>
          <?php foreach ($no_kan as $kan) : ?>
          <div class="row">
            <div class="col-md-2">
              <i style="font-size: 20px;" class="fas fa-phone"></i>
            </div>
            <div class="col-md-10">

                <div>Kantor &nbsp; : <span style="color: #0984e3">
                  <?php
                    $sps1 = substr($kan->text_content, 0,4);
                    $sps2 = substr($kan->text_content, 4,4);
                    $sps3 = substr($kan->text_content, 8,4);
                    $kan->text_content = $sps1 . " " . $sps2 . " " . $sps3;
                   ?>
                  <?= $kan->text_content; ?>
                </span>
              </div>
            </div>
          </div>
        <?php endforeach; ?><br>
          <?php foreach ($email as $email) : ?>
          <div class="row">
            <div class="col-md-2">
              <i style="font-size: 20px;" class="fas fa-envelope"></i>
            </div>
            <div class="col-md-10">
                <p style="color: #0984e3"><?php echo $email->text_content; ?></p>
            </div>
          </div>
          <?php endforeach; ?>
        </div>


        <div class="col-md-4">
          <br>
          <div class="row">
            <div class="col-md-12">
              <img src="assets/img/logos/zamasco-image.png" width="60%">
            </div>
          </div><br><br>
          <div class="row">
            <div class="col-md-12">
              <?php foreach ($cont as $content) : ?>
                <p><?php echo $content->text_content; ?></p>
              <?php endforeach; ?>
            </div>
          </div>

        </div>

      </div>
    </div>
  </div>
