
<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="4000" style=" height: 100%">

	  <div class="carousel-inner">


	    <?php foreach($banners as $banner): ?>
			<div class="item kenburn" id="trig" style="background-image: url(<?= substr($banner->picture_dir,7).$banner->picture ?>);background-size: cover;background-position: center;transform: scale(1.2););">
				<div style="height: 700px">
					<div class="container-fluid">
						<div class="col-md-8 col-md-offset-2" style="
							<?php if($no % 2 == 0): ?>
								text-align: center;
							<?php elseif ($no % 3 == 0): ?>
								text-align: right;
							<?php else: ?>
								text-align: left;
							<?php endif; ?>
						">

							<div style="margin-top: 45%; color: black;" id="title-content">
								<h1 class="animated
								<?php if($no % 2 == 0): ?>
									zoomIn
								<?php elseif ($no % 3 == 0): ?>
									fadeInRight
								<?php else: ?>
									fadeInLeft
								<?php endif; ?>
								 animation-delay-100" style="color: white; transform: scale(1);font-weight: bold;"><?= $banner->title ?></h1>
								<p class="lead animated
								<?php if($no % 2 == 0): ?>
									zoomIn
								<?php elseif ($no % 3 == 0): ?>
									fadeInRight
								<?php else: ?>
									fadeInLeft
								<?php endif; ?>
								 animation-delay-200" style="color: white; font-size: 30px;font-weight: bold;"><?= $banner->text_content ?></p>
							</div>
						</div>
					</div>
				</div>

		    </div>
		    <?php $no++ ?>
	    <?php endforeach; ?>

		<script>
			$(document).ready(function(){
				$('#trig').addClass('active');
			});
		</script>


	</div>
</div>



	<div class="section-block" style="padding-bottom: 0;">
		<div class="container">
			<div class="row" style="margin-top: -160px; z-index: 999 !important;">
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="project-item project-fix">
						<?php foreach ($ehealth as $eh) : ?>
							<img src="<?= substr($eh->picture_dir,7).$eh->picture; ?>" alt="blog-img">
							<div class="project-item-overlay">
								<div class="project-item-content">
									<h5>E-Health</h5>
									<p style="color: #fff; margin-top: 10px;">
										<?= $eh->text_content; ?>
									</p>
									<a href="<?= $this->url->build(['action'=>'product','#'=>'ehealth']) ?>">
										View More
									</a>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="project-item project-fix">
						<?php foreach ($consultant as $cons) : ?>
							<img src="<?= substr($cons->picture_dir, 7).$cons->picture; ?>" alt="blog-img">
							<div class="project-item-overlay">
								<div class="project-item-content">
									<h5>Consultant</h5>
									<p style="color: #fff; margin-top: 10px;">
										<?= $cons->text_content; ?>
									</p>
									<a href="<?= $this->url->build(['action'=>'product','#'=>'consultant']) ?>">
										View More
									</a>
									</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="project-item project-fix">
						<?php foreach ($technology as $tech) : ?>
							<img src="<?= substr($tech->picture_dir, 7).$tech->picture; ?>" alt="blog-img">
							<div class="project-item-overlay">
								<div class="project-item-content">
									<h5>Technology</h5>
									<p style="color: #fff; margin-top: 10px;">
										<?= $tech->text_content; ?>
									</p>
									<a href="<?= $this->url->build(['action'=>'product','#'=>'it']) ?>">
										View More
									</a>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="section-block">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="section-heading-left">
						<h2>SIAPA KAMI</h2>
					</div>
					<?php foreach ($videos as $vid) : ?>
					<div class="product-box" style="margin-top: 30px;">
						<div class="product-img">
							<?php
								$video = explode('=',$vid);
							 ?>
							<iframe width="100%" height="315" src="https://www.youtube.com/embed/<?= $video[1] ?>" frameborder="0" allowfullscreen></iframe>
						</div>
						<div class="product-info">
							<p>
								<?= $vid->text_content; ?>
							</p>
						</div>
					</div>
				<?php endforeach; ?>
				</div>
				<div class="col-md-6">
					<div class="section-heading-left">
						<h2>PRODUK UNGGULAN KAMI</h2>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="project-carousel">
								<div class="owl-carousel owl-theme"id="project-detail">

									<?php foreach($products as $product): ?>
										<a href="<?= $this->url->build(['action'=>'detail','?'=>['id'=>$product->id,'pages'=>'product']]) ?>">
											<div class="services-box" style="border: none; height: 400px;">
												<img src="<?= substr($product->picture_dir,7).$product->picture ?>" alt="blog-img" id="<?= $product->id ?>">
												<div class="services-box-inner">
													<h4><?= $product->name ?></h4>
													<p>
														<?= substr($product->content,0,80).'...'; ?>
													</p>
												</div>
											</div>
											<script type="text/javascript">
												$('img').load(function(){
													if($('#<?= $product->id ?>').height() < 200){
														var height = $('#<?= $product->id ?>').height();
														console.log(height);
															$('#<?= $product->id ?>').css({'margin-top' : (233-height)/4 + 'px','margin-bottom' : (233-height)/4 + 'px'});
													}
												});
											</script>
										</a>

									<?php endforeach; ?>
									<!-- <div class="services-box" style="border: none; height: 520px;">
										<img src="assets/img/content/projects/toroz2.jpg" alt="blog-img">
										<div class="services-box-inner">
											<h4>Toroz Teleradiologi</h4>
											<p>
												Aplikasi yang berbasih Web dan Mobile Android ini dapat membantu para Dokter untuk memberi Diagnosa jarak jauh, dengan mengacu pada data-data yang ditampilkan oleh aplikasi ini dengan akurat...
											</p>
										</div>
									</div>
									<div class="services-box" style="border: none; height: 520px;">
										<img src="assets/img/content/projects/qc-30.jpg" alt="blog-img">
										<div class="services-box-inner">
											<h4>Car Wash System</h4>
											<p>
												Saat ini perkembangan populasi kendaraan di Indonesia sangat signifikan, mengacu pada hal tesebut maka peluang untuk melakukan bisnis terkait...
											</p>
										</div>
									</div> -->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="section-block-grey" style="margin-top: -60px;">
		<div class="container">
			<div class="section-heading-left">
				<h2>BUTUH SOLUSI? KAMI ADALAH MITRA YANG TEPAT</h2>
			</div>
			<div class="row">
				<div class="col-md-3">
					<div class="contact-left-side">
						<div class="contact-left-box">
							<div class="contact-box-full clearfix">
								<div class="contact-icon">
									<i class="fa fa-code"></i>
								</div>
								<?php foreach ($footer1 as $foot1) : ?>
									<div class="contact-info">
										<h5><?= $foot1->title; ?></h5>
										<p><?= $foot1->text_content; ?></p>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="contact-left-side">
						<div class="contact-left-box">
							<div class="contact-box-full clearfix">
								<div class="contact-icon">
									<i class="fa fa-angle-double-up"></i>
								</div>
								<?php foreach ($footer2 as $foot2) : ?>
									<div class="contact-info">
										<h5><?= $foot2->title; ?></h5>
										<p><?= $foot2->text_content; ?></p>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="contact-left-side">
						<div class="contact-left-box">
							<div class="contact-box-full clearfix">
								<div class="contact-icon">
									<i class="fa fa-book"></i>
								</div>
								<?php foreach ($footer3 as $foot3) : ?>
									<div class="contact-info">
										<h5><?= $foot3->title; ?></h5>
										<p><?= $foot3->text_content; ?></p>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="contact-left-side">
						<div class="contact-left-box">
							<div class="contact-box-full clearfix">
								<div class="contact-icon">
									<i class="fas fa-id-card"></i>
								</div>
								<?php foreach ($footer4 as $foot4) : ?>
									<div class="contact-info">
										<h5><?= $foot4->title; ?></h5>
										<p><?= $foot4->text_content; ?></p>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php $this->start('script') ?>
		<script>




			$('#project-detail').owlCarousel({
			    loop: true,
			    nav: false,
			    dots: false,
			    autoplay: true,
			    autoplayTimeout: 3000,
			    responsiveClass: true,
			    autoplayHoverPause:false,
			    responsive: {
			      0: {
			        items: 1,
			        margin: 10,
			      },
			      600: {
			        items: 2,
			        margin: 25,
			      },
			      1000: {
			        items: 2,
			        margin: 25,
			      }
			    }
			  })
		</script>
	<?php $this->end() ?>
