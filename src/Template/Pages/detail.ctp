<div class="owl-carousel owl-theme" id="banner">
<?php foreach($banners as $banner): ?>
  <div class="page-title " style="background-image: url(<?= substr($banner->picture_dir,7).$banner->picture ?>); background-position: center;">
    <div class="container">
      <h1><?= ucfirst(strtolower($banner->pages))." Detail" ?></h1>
      <ul>
        <li><a href="<?= $this->url->build(['action'=>'index']) ?>">Home</a></li>
        <li><a href="<?= $this->url->build(['action'=>strtolower($banner->pages)]) ?>"><?= ucfirst(strtolower($banner->pages)) ?></a></li>
        <li><a href="<?= $this->url->build($this->request->here()) ?>"><?= ucfirst(strtolower($banner->pages))." Detail" ?></a></li>
      </ul>
    </div>
  </div>
<?php endforeach; ?>
</div>

<?php if($pages == 'product'): ?>
  <?php
    foreach ($details as $detail) {
      // code...
    }
   ?>
  <div class="section-block">
    <div class="container">

      <div class="row">

        <div class="col-md-6 col-md-push-6">
          <img src="<?= substr($detail->picture_dir, 7).$detail->picture ?>" alt="">
        </div>
        <div class="col-md-6 col-md-pull-6">
          <div class="section-heading-left">
            <h2><?= $detail->name ?></h2>
          </div>
          <span class="label label-primary" title="DIVISI : <?= strtoupper($detail->division) ?>"><?= strtoupper($detail->division) ?></span>
          <span class="label label-success" title="KATEGORI : <?= strtoupper($detail->category) ?>"><?= strtoupper($detail->category) ?></span>
          <br><br>
          <?= $detail->content ?>

          <a href="<?= $this->url->build(['action'=>'contact']) ?>" class="btn btn-primary pull-right">Pesan Sekarang</a>
        </div>
      </div>
    </div>
  </div>
  <br><br><br><br>
<?php elseif($pages == 'gallery'): ?>
  <?php
    foreach ($albums as $album) {
      // code...
    }
  ?>
  <div class="section-block">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="section-heading">
						<center><h2><?= $album->title ?></h2></center>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div id="container">
            <div id="mygallery" >

              <?php foreach ($details as $detail): ?>
                <div class="photo item">
                  <img src="<?= substr($detail->path, 7).$detail->name ?>" >

                </div>
              <?php endforeach; ?>

            </div>
						
					</div>
				</div>
			</div>

		</div>
	</div>

  <?php $this->start('script') ?>
  <script>
    $("#mygallery").justifiedGallery({
      rowHeight: 200,
      lastRow: 'nojustify',
      randomize: true,
      margins: 5
    });
  </script>
  <?php $this->end() ?>
<?php endif; ?>
