

	<div class="owl-carousel owl-theme" id="banner">
	<?php foreach($banners as $banner): ?>
		<div class="page-title" style="background-image: url(<?= substr($banner->picture_dir,7).$banner->picture ?>); background-position: center;">
			<div class="container">
				<h1>About</h1>
				<ul>
					<li><a href="/">Home</a></li>
					<li><a href="/about">About</a></li>
				</ul>
			</div>
		</div>
		<?php endforeach; ?>
	</div>

	<div class="section-block">
		<div class="container-fluid">
			<div class="row">

				<!-- Introduce -->

				<div class="container">
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="col-md-4 col-xs-10">
								<div class="section-heading-left">
									<h2>Pengenalan</h2>
								</div>
								<div class="col-md-4 col-xs-4"><br><br>
									<img src="assets/img/logo.png" class="img-responsive img-rounded gray">
								</div>
								<?php foreach ($intro as $intro) : ?>
									<div class="col-md-8 col-xs-8">
										<p>
											<?= $intro->text_content; ?>
										</p>
									</div>
								<?php endforeach; ?>
							</div>
							<div class="col-md-4 col-xs-10">
								<div class="section-heading-left">
									<h2>Visi</h2>
								</div>
								<div class="col-md-4 col-xs-4"><br><br>
									<img src="assets/img/Vision.png" class="img-responsive img-rounded gray">
								</div>
								<?php foreach ($visi as $visi) : ?>
									<div class="col-md-8 col-xs-8">
										<p>
											<?= $visi->text_content; ?>
										</p>
									</div>
								<?php endforeach; ?>
							</div>
							<div class="col-md-4 col-xs-10">
								<div class="section-heading-left">
									<h2>Misi</h2>
								</div>
								<div class="col-md-4 col-xs-4"><br>
									<img src="assets/img/row-crop.jpg" class="img-responsive img-rounded gray">
								</div>
								<?php foreach ($misi as $misi) : ?>
									<div class="col-md-8 col-xs-8">
										<p>
											<?= $misi->text_content; ?>
										</p>
									</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div><br><br>

				<!-- end Introfuce -->

				<!-- Division -->

				<div class="row">
					<div id="img-1" style="background-attachment: fixed;" class="img-responsive">
						<div class="container" data-aos="fade-down" data-aos-delay="500">
							<br>
							<div class="row">
								<div class="col-md-12">
									<h1 style="color: white; font-size: 45px; text-align: center;">Technology</h1>
								</div>
							</div>

							<div class="row">
								<?php foreach ($tech as $tech) : ?>
									<div class="col-md-12">
										<br>
										<p style="font-size: 18px;">
											<?= $tech->text_content; ?>
										</p>
									</div>
								<?php endforeach; ?>
							</div>
							<div class="row">
								<div class="col-md-12">
									<br><br><br>

										<div class="text-right">
											<img src="assets/img/content/about/Picture1.png" width="80"> &nbsp;&nbsp;
											<img src="assets/img/content/about/laravel-2.png" width="80"> &nbsp;&nbsp;
											<img src="assets/img/content/about/Picture3.png" width="80"> &nbsp;&nbsp;
											<img src="assets/img/content/about/Picture4.png" width="80"> &nbsp;&nbsp;
											<img src="assets/img/content/about/Picture5.png" width="80"> &nbsp;&nbsp;
											<img src="assets/img/content/about/Picture7.png" width="80"> &nbsp;&nbsp;
											<img src="assets/img/content/about/Picture8.png" width="80"> &nbsp;&nbsp;
											<img src="assets/img/content/about/Picture9.png" width="70"> &nbsp;&nbsp;
											<img src="assets/img/content/about/Picture10.png" width="65"> &nbsp;&nbsp;
											<img src="assets/img/content/about/Picture11.png" width="50"> &nbsp;&nbsp;
											<img src="assets/img/content/about/Picture12.png" width="50"> &nbsp;&nbsp;
											<img src="assets/img/content/about/android.png" width="50">
										</div>
								</div>
							</div>
						</div><br><br><br>
					</div>
				</div>
				<div class="row">
					<div id="img-2" style="background-attachment: fixed;" class="img-responsive">
						<br><br>

						<div class="container" data-aos="fade-down" data-aos-delay="500">

							<div class="row">
								<div class="col-md-12">
									<h1 style="color: white; font-size: 45px; text-align: center;">E - Health</h1>
								</div>
							</div>

							<div class="row">
								<?php foreach ($ehealth as $eh) : ?>
									<div class="col-md-12 text-left">
										<br>
										<p style="font-size: 18px;">
											<?= $eh->text_content; ?>
										</p>
									</div>
								<?php endforeach; ?>
							</div>
							<div class="row">
								<div class="col-md-12">
									<br>
										<!-- <div style="text-align: center; font-size: 30px;">Our Partner</div> -->
									<br><br>

									<div class="text-right">
										<img src="assets/img/content/about/zte.png" width="80">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<img src="assets/img/content/about/eztalks.png" width="200">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										<img src="assets/img/content/about/ecatalog.png" width="180">
									</div>
								</div>
							</div>
							
						</div><br><br><br><br>
					</div>
				</div>
				<div class="row">
					<div id="img-3" style="background-attachment: fixed;" class="img-responsive">
						<br>

						<div class="container" data-aos="fade-down" data-aos-delay="500">

							<div class="row">
								<div class="col-md-12">
									<h1 style="color: white; font-size: 45px; text-align: center;">Consultant</h1>
								</div>
							</div>
							
							<div class="row">
								<?php foreach ($consultant as $cons) : ?>
									<div class="col-md-12">
										<br>
										<p style="font-size: 18px;">
											<?= $cons->text_content; ?>
										</p>
									</div>
								<?php endforeach; ?>
							</div>
							<div class="row">
								<div class="col-md-12">
									<br>
										<!-- <div style="text-align: center; font-size: 30px;">Collaborate</div> -->
									<br><br>

									<div class="text-right">
										<img src="assets/img/content/about/ikkesin.jpg" width="100">
									</div>

								</div>
							</div>

						</div><br><br><br><br>
					</div>
				</div><br><br>

				<!-- end Division -->

				<!-- who we are -->

				<div class="container">
					<div style="text-align: center;" class="section-heading">
						<h2>Kenapa Harus Kami</h2>
					</div>
					<div class="row" style="margin-top: -40px;>
						<?php foreach ($moto as $moto) : ?>
							<div class="col-md-6 col-md-offset-3">
								<p style="text-align: center;">
									<?= $moto->text_content; ?>
								</p>
							</div>
						<?php endforeach; ?>
					</div><br><br>
					<div class="row" style="border-right: 1px solid black;">
						<div class="col-md-12">
							<div class="col-md-4" style="text-align: center;">
								<i style="font-size: 75px;" class="fa fa-user-secret"></i>
								<?php foreach ($moto1 as $moto1) : ?>
									<h3><?= $moto1->title; ?></h3>
									<p>
										<?= $moto1->text_content; ?>
									</p>
								<?php endforeach; ?>
							</div>
							<div class="col-md-4" style="text-align: center;">
								<i style="font-size: 75px" class="fa fa-american-sign-language-interpreting"></i>
								<?php foreach ($moto2 as $moto2) : ?>
									<h3><?= $moto2->title; ?></h3>
									<p>
										<?= $moto2->text_content; ?>
									</p>
								<?php endforeach; ?>
							</div>
							<div class="col-md-4" style="text-align: center;">
								<i style="font-size: 75px" class="fa fa-crosshairs"></i>
								<?php foreach ($moto3 as $moto3) : ?>
									<h3><?= $moto3->title; ?></h3>
									<p>
										<?= $moto3->text_content; ?>
									</p>
								<?php endforeach; ?>
							</div>
						</div>
					</div><br><br>
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-4" style="text-align: center;">
								<i style="font-size: 75px;" class="fa fa-address-card"></i>
								<?php foreach ($moto4 as $moto4) : ?>
									<h3><?= $moto4->title; ?></h3>
									<p>
										<?= $moto4->text_content; ?>
									</p>
								<?php endforeach; ?>
							</div>
							<div class="col-md-4" style="text-align: center;">
								<i style="font-size: 75px" class="fa fa-link"></i>
								<?php foreach ($moto5 as $moto5) : ?>
									<h3><?= $moto5->title; ?></h3>
									<p>
										<?= $moto5->text_content; ?>
									</p>
								<?php endforeach; ?>
							</div>
							<div class="col-md-4" style="text-align: center;">
								<i style="font-size: 75px" class="fa fa-key"></i>
								<?php foreach ($moto6 as $moto6) : ?>
									<h3><?= $moto6->title; ?></h3>
									<p>
										<?= $moto6->text_content; ?>
									</p>
								<?php endforeach; ?>
							</div>
						</div>
					</div>
				</div>

				<!-- end who we are -->

			</div>
		</div>
	</div><br>

	<div class="section-block-grey">

		<!-- Team -->

			<div class="container">
				<div style="text-align: center;" class="section-heading">
					<h2>Tim Kami</h2>
				</div>
				<div class="row">

					<?php foreach ($anggota as $agg) : ?>
						<div class="col-md-4">
							<div class="row">
							  <div class="col-md-12">
							    <div class="thumbnail">
							      <img src="<?= substr($agg->picture_dir, 7).$agg->picture; ?>">
							      <div class="caption">
							        <h4 class="text-center"><?= $agg->title; ?></h4>
							        <hr>
							        <center>
							        	<p style="color: #8395a7; font-size: 15px;"><?= $agg->text_content; ?></p>
							        </center>	
									<br><br>
							      </div>
							    </div>
							  </div>
							</div>
						<br><br>
						</div>
					<?php endforeach; ?>

					<br><br>

					<!-- <div class="col-md-12">
						<div class="row">
							<?php foreach ($komisaris as $kom) : ?>
								<div class="col-md-4">
									<img src="<?= substr($kom->picture_dir, 7).$kom->picture; ?>" width="100%" class="img-responsive img-rounded">
								</div>
								<div class="col-md-4" style="padding-top: 5%;">
									<h4 style="font-size: 20px; text-align: center;"><?= $kom->title; ?></h4><hr>
									<p style="color: #8395a7; font-size: 20px; text-align: center;">Commissioner</p>
									<br><br>
									<p class="text-center">
										<?= $kom->text_content; ?>
									</p>
								</div>
							<?php endforeach; ?>
							<div class="col-md-4">
								<div class="diamondswrap text-center">
								    <?php foreach ($anggota as $ang) : ?>
								    	<div class="item">
									    	<a href="#" data-izimodal-open="#<?= $ang->id; ?>" data-izimodal-transitionin="fadeInDown" data-izimodal-zindex="20000">
									    		<img src="<?= substr($ang->picture_dir,7).$ang->picture ?>">
									    	</a>
									    </div>
								    <?php endforeach; ?>
							    </div>
							</div>
						</div>
					</div> -->
				</div>

				<!-- <?php foreach ($anggota as $ang) : ?>
					<div class="modal-custom" data-izimodal-group="team" id="<?= $ang->id; ?>" data-iziModal-fullscreen="false"  data-iziModal-title="<?= $ang->title; ?>"  data-iziModal-subtitle="<?= $ang->text_content; ?>">
						<img src="<?= substr($ang->picture_dir, 7).$ang->picture ?>" width="100%">
					</div>
				<?php endforeach; ?> -->

			</div>

		<!-- end -->

	</div>

	<div class="section-block">
		<div class="container">
			<div style="text-align: center;" class="section-heading">
				<h2>Seluruh Tim Kami</h2>
			</div>
			<div class="row">
				<?php foreach ($foteam as $fot) : ?>
					<div class="col-md-8">
						<img src="<?= substr($fot->picture_dir, 7).$fot->picture; ?>" width="100%" class="img-rounded">
					</div>
				<?php endforeach; ?>
				<div class="col-md-4 ngasal">
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quibusdam nisi asperiores, corrupti suscipit quisquam sed doloremque, a nihil aspernatur numquam harum. Iusto officia fugit voluptatum fugiat vitae quis. Dolor, esse!
					</p> 
				</div>
			</div>
		</div>
	</div>
	<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
	<?php $this->start('script') ?>
		<script>
			$('#banner').owlCarousel({
			    items:1,
			    smartSpeed:450,
			    loop: true,
			    autoplay:true,
			    autoplayTimeout:4000,
			    autoplayHoverPause:true,
			    lazyLoad: true,
			    dots: false,
			    autoHeight:true
			        // autoWidth:true,
			  });

			$(document).ready(function(){
		      $(".modal-custom").iziModal({
		        headerColor: 'rgba(0,0,0, .8)',
		        background: 'transparent',
		        arrowKeys: true,
		        borderBottom: false,
		        navigateArrows: true,
		        navigateCaption: false,
		        zindex: 30000
		      });

		      $(".diamondswrap").diamonds({
		          size: 130, // Size of the squares
		          gap: 6 // Pixels between squares
		      });

		       AOS.init();

		        lightbox.option({
		            'resizeDuration': 200,
		            'wrapAround': true
		        })
		    });
		</script>
	<?php $this->end(); ?>
