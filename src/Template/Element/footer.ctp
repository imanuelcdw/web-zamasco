<div class="footer-bar" style="z-index: 9999">
		<div class="row">
			<div class="col-md-4">
				<span class="left">© Copyright PT Zamasco Mitra Solusindo - Jakarta, Indonesia</span>
			</div>
			<div class="col-md-4">
				
			</div>
			<div class="col-md-4">
				<span><a href="https://www.facebook.com/zamasco" target="_blank" style="color: #fff; padding: 0px 10px 0 10px;"><i class="fab fa-facebook" style="font-size: 25px;"></i></a></span>
				<span><a href="https://www.instagram.com/zamascoms/" target="_blank" style="color: #fff; padding: 0px 10px 0 10px;"><i class="fab fa-instagram" style="font-size: 25px;"></i></a></span>
				<span><a href="https://www.youtube.com/channel/UClHDrkv9ypmRtFeBkar1Nfw" target="_blank" style="color: #fff; padding: 0px 10px 0 10px;"><i class="fab fa-youtube" style="font-size: 25px;"></i></a></span>
			</div>
		</div>
	</div>