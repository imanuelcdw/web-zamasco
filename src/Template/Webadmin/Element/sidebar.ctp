    <!-- BEGIN: Left Aside -->
    <?php
        $url = $this->request->here;
      ?>
    <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
    <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
        <!-- BEGIN: Aside Menu -->
        <div
            id="m_ver_menu"
            class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
            m-menu-vertical="1"
            m-menu-scrollable="1" m-menu-dropdown-timeout="500"
            style="position: relative;">
            <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
               <li class="m-menu__section">
                    <h4 class="m-menu__section-text">
                        Accessibility
                    </h4>
                    <i class="m-menu__section-icon flaticon-more-v3"></i>
                </li>
                    <li class="m-menu__item <?= ($url == $this->Url->build(['controller'=>'Dashboard']) ? 'm-menu__item--active' : '' ) ?>" aria-haspopup="true" >
                        <a  href="<?= $this->Url->Build(['controller'=>'Dashboard','action'=>'index']) ?>" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-line-graph"></i>
                            <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">
                                        Dashboard
                                    </span>
                                </span>
                            </span>
                        </a>
                    </li>
                    <li class="m-menu__item <?= ($url == $this->Url->build(['controller'=>'Groups']) ? 'm-menu__item--active' : '' ) ?>" aria-haspopup="true" >
                        <a  href="<?= $this->Url->Build(['controller'=>'Groups','action'=>'index']) ?>" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-users"></i>
                            <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">
                                        Groups
                                    </span>
                                </span>
                            </span>
                        </a>
                    </li>



                    <li class="m-menu__item <?= ($url == $this->Url->build(['controller'=>'Users']) ? 'm-menu__item--active' : '' ) ?>" aria-haspopup="true" >
                        <a  href="<?= $this->Url->Build(['controller'=>'Users','action'=>'index']) ?>" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-user-ok"></i>
                            <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">
                                        Users
                                    </span>
                                </span>
                            </span>
                        </a>
                    </li>



                <!--  -->

                    <li class="m-menu__section">
                        <h4 class="m-menu__section-text">
                            Master
                        </h4>
                        <i class="m-menu__section-icon flaticon-more-v3"></i>
                    </li>

                    <li class="m-menu__item <?= ($url == $this->Url->build(['controller'=>'Contents']) ? 'm-menu__item--active' : '' ) ?>" aria-haspopup="true" >
                        <a href="<?= $this->Url->Build(['controller'=>'Contents','action'=>'index']) ?>" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-line-graph"></i>
                            <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">
                                        Contents
                                    </span>
                                </span>
                            </span>
                        </a>
                    </li>

                    <li class="m-menu__item <?= ($url == $this->Url->build(['controller'=>'Covers']) ? 'm-menu__item--active' : '' ) ?>" aria-haspopup="true" >
                        <a href="<?= $this->Url->Build(['controller'=>'Covers','action'=>'index']) ?>" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-tabs"></i>
                            <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">
                                        Banner
                                    </span>
                                </span>
                            </span>
                        </a>
                    </li>

                    <li class="m-menu__item <?= ($url == $this->Url->build(['controller'=>'Products']) ? 'm-menu__item--active' : '' ) ?>" aria-haspopup="true" >
                        <a href="<?= $this->Url->Build(['controller'=>'Products','action'=>'index']) ?>" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-business"></i>
                            <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">
                                        Products
                                    </span>
                                </span>
                            </span>
                        </a>
                    </li>

                    <li class="m-menu__item <?= ($url == $this->Url->build(['controller'=>'Albums']) ? 'm-menu__item--active' : '' ) ?>" aria-haspopup="true" >
                        <a href="<?= $this->Url->Build(['controller'=>'Albums','action'=>'index']) ?>" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-web"></i>
                            <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">
                                        Albums
                                    </span>
                                </span>
                            </span>
                        </a>
                    </li>

                    <li class="m-menu__item <?= ($url == $this->Url->build(['controller'=>'Pictures']) ? 'm-menu__item--active' : '' ) ?>" aria-haspopup="true" >
                        <a href="<?= $this->Url->Build(['controller'=>'Pictures','action'=>'index']) ?>" class="m-menu__link ">
                            <i class="m-menu__link-icon flaticon-add"></i>
                            <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap">
                                    <span class="m-menu__link-text">
                                        Pictures
                                    </span>
                                </span>
                            </span>
                        </a>
                    </li>



            </ul>
        </div>
        <!-- END: Aside Menu -->
    </div>
    <!-- END: Left Aside -->
