<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Content $content
 */
?>

<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'index']);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>
                </li>
            </ul>
        </div>
    </div>
    <?= $this->Form->create($content,['class'=>'m-form m-form--fit m-form--label-align-right','type'=>'file']) ?>
        <div class="m-portlet__body">
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('page', ['type' => 'select', 'options' => ['index' => 'Home', 'tentang'=>'About', 'kontak'=>'Contact']]);?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('position', ['type' => 'select']);?>
                </div>
                <div class="col-md-4">
                    <?= $this->Form->control('link') ?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('title');?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('status',['options'=>['DISABLED','ENABLED'],'default'=>0]);?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('picture', ['class'=>'m-input form-control','type'=>'file','label'=>'*Picture']);?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('text_content', ['class' => 'form-control','label'=>'*Text Content']);?>
                </div>
            </div>


        </div>


        <div class="m-form__actions m-form__actions--sm m-form__actions--solid">
            <button type="submit" class="btn btn-primary">
                Submit
            </button>
            <button type="reset" class="btn btn-secondary">
                Cancel
            </button>
        </div>
    <?= $this->Form->end();?>
</div>



<script>

    $('#page').append('<option selected disable first></option>');

    $('#page').on('change', function() {
        var INDEX = {
            'video' : 'Video',
            'ehealth' : 'E-Health Division',
            'consultant' : 'Consultant Division',
            'technology' : 'Technology Division',
            'footer1' : 'Code Footer',
            'footer2' : 'Spirit Footer',
            'footer3' : 'Book Footer',
            'footer4' : 'Identify Footer'
        };


        var TENTANG = {
            'intro' : 'Intro',
            'visi' : 'Visi',
            'misi' : 'Misi',
            'technology' : 'Divisi Tech',
            'ehealth' : 'Divisi E-Health',
            'consultant' : 'Divisi Consultant',
            'moto' : 'Title Moto',
            'moto1' : 'Moto 1',
            'moto2' : 'Moto 2',
            'moto3' : 'Moto 3',
            'moto4' : 'Moto 4',
            'moto5' : 'Moto 5',
            'moto6' : 'Moto 6',
            'komisaris' : 'Commissioner',
            'anggota' : 'Foto Director / Manager',
            'foteam' : 'Foto Team'
        };

        var KONTAK = {
            'alamat' : 'Alamat',
            'tlpn1' : 'Telephone Kantor',
            'tlpn2' : 'Telephone Seluler',
            'email' : 'email',
            'intro' : 'Intro Zamasco'
        };

        var select = $('#position');
        if (select.prop) {
            var options = select.prop('options');
        }
        else{
            var options = select.attr('options');
        }
        $('option', select).remove();

        if (this.value == "index") {
            $.each(INDEX, function(val, text) {
                options[options.length] = new Option(text, val);
            });
        }else if(this.value == "tentang"){
            $.each(TENTANG, function(val, text){
                options[options.length] = new Option(text, val);
            });
        }else if(this.value == "kontak") {
            $.each(KONTAK, function(val, text){
               options[options.length] = new Option(text, val);
            });
        }


    });

</script>
