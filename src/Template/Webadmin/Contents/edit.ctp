<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Content $content
 */
?>

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Content $content
 */
?>

<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'index']);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>
                </li>
            </ul>
        </div>
    </div>
    <?= $this->Form->create($content,['class'=>'m-form m-form--fit m-form--label-align-right','type'=>'file']) ?>
        <div class="m-portlet__body">
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('page');?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('position');?>
                </div>
                <div class="col-md-4">
                    <?= $this->Form->control('link');  ?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('title');?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('text_content', ['class' => 'form-control']);?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('status',['options'=>['DISABLED','ENABLED'],'default'=>0]);?>
                </div>
                <div class="col-md-4">
                    <!-- <label for="picture">Picture</label> -->
                    <div class="row">
                        <div class="col-md-8">
                            <?=$this->Form->control('picture', ['class'=>'m-input form-control','type'=>'file']);?>
                        </div>
                        <div class="col-md-4">
                            <img src="<?= substr($content->picture_dir, 7).$content->picture ?>" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="m-form__actions m-form__actions--sm m-form__actions--solid">
            <button type="submit" class="btn btn-primary">
                Submit
            </button>
            <button type="reset" class="btn btn-secondary">
                Cancel
            </button>
        </div>
    <?= $this->Form->end();?>
</div>
