<?= $this->Form->create(null,['class'=>'m-login__form m-form']);?>
    <?=$this->Flash->render();?>
    <?=$this->Form->controls([
        'username' => ['label'=>false,'autocomplete'=>'off','placeholder'=>'Username'],
    ],[
        'legend'=>false,
        'fieldset' => false
    ]);?>
    <?php
        $this->Form->setTemplates([
            'input' => '<input type="{{type}}" name="{{name}}" {{attrs}}/>',
        ]);
    ?>
    <?=$this->Form->controls([
        'password' => ['label'=>false,'autocomplete'=>'off','placeholder'=>'Password','class'=>'form-control m-input m-login__form-input--last',],
    ],[
        'legend'=>false,
        'fieldset' => false
    ]);?>
    <div class="row m-login__form-sub">
        <div class="col m--align-left m-login__form-left">
            <label class="m-checkbox  m-checkbox--light">
                <?=$this->Form->checkbox('remember', ['value' => 1]);?> Remember me
                <span></span>
            </label>
        </div>
        <!-- <div class="col m--align-right m-login__form-right">
            <a href="javascript:;" id="m_login_forget_password" class="m-link">Forget Password ?</a>
        </div> -->
    </div>
    <div class="m-login__form-action">
        <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn">Sign In</button>
    </div>
<?= $this->Form->end();?>