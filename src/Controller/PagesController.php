<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\I18n\Time;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\TableRegistry;
use Cake\Network\Exception\NotFoundException;
use Cake\Mailer\Email;


class PagesController extends AppController
{
   public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        if(php_sapi_name() !== 'cli'){
            $this->Auth->allow(['index','about','product','gallery','portfolio','contact','detail']);
        }
    }

   public function index(){
     $cover = TableRegistry::get('Covers');
     $content = TableRegistry::get('Contents');
     $product = TableRegistry::get('Products');
     $banners = $cover->find()->where("pages ='HOME' AND status = '1'");

     $products = $content->find()->where("position ='products' AND status = '1'");
     $videos = $content->find()->where("page = 'index' AND position = 'video' AND status = '1'");
     $ehealth = $content->find()->where("page = 'index' AND position = 'ehealth' AND status = '1'");
     $consultant = $content->find()->where("page = 'index' AND position = 'consultant' AND status = '1'");
     $technology = $content->find()->where("page = 'index' AND position = 'technology' AND status = '1'");
     $footer1 = $content->find()->where("page = 'index' AND position = 'footer1' AND status = '1'");
     $footer2 = $content->find()->where("page = 'index' AND position = 'footer2' AND status = '1'");
     $footer3 = $content->find()->where("page = 'index' AND position = 'footer3' AND status = '1'");
     $footer4 = $content->find()->where("page = 'index' AND position = 'footer4' AND status = '1'");
     $products = $product->find()->where("status = '1'");
     $videos = $content->find()->where("page = 'index' AND position = 'video' AND status = '1'");

     $no = 1;
     $this->set(compact('banners','products', 'videos', 'ehealth', 'consultant', 'technology', 'footer1', 'footer2', 'footer3', 'footer4','no'));
   }

   public function about(){
     $cover = TableRegistry::get('Covers');
     $content = TableRegistry::get('Contents');

     $banners = $cover->find()->where("pages ='ABOUT' OR pages = 'ALL' AND status = '1'");
     $this->set(compact('banners'));

     $intro = $content->find()->where("page = 'tentang' AND position = 'intro' AND status = '1'");
     $visi = $content->find()->where("page = 'tentang' AND position = 'visi' AND status = '1'");
     $misi = $content->find()->where("page = 'tentang' AND position = 'misi' AND status = '1'");
     $tech = $content->find()->where("page = 'tentang' AND position = 'technology' AND status = '1'");
     $ehealth = $content->find()->where("page = 'tentang' AND position = 'ehealth' AND status = '1'");
     $consultant = $content->find()->where("page = 'tentang' AND position = 'consultant' AND status = '1'");
     $moto = $content->find()->where("page = 'tentang' AND position = 'moto' AND status = '1'");
     $moto1 = $content->find()->where("page = 'tentang' AND position = 'moto1' AND status = '1'");
     $moto2 = $content->find()->where("page = 'tentang' AND position = 'moto2' AND status = '1'");
     $moto3 = $content->find()->where("page = 'tentang' AND position = 'moto3' AND status = '1'");
     $moto4 = $content->find()->where("page = 'tentang' AND position = 'moto4' AND status = '1'");
     $moto5 = $content->find()->where("page = 'tentang' AND position = 'moto5' AND status = '1'");
     $moto6 = $content->find()->where("page = 'tentang' AND position = 'moto6' AND status = '1'");
     $anggota = $content->find()->where("page = 'tentang' AND position = 'anggota' AND status = '1'");
     $foteam = $content->find()->where("page = 'tentang' AND position = 'foteam' AND status = '1'");

     $this->set(compact('intro', 'visi', 'misi', 'tech', 'ehealth', 'consultant', 'moto', 'moto1','moto2','moto3','moto4','moto5','moto6', 'komisaris', 'anggota', 'foteam'));

   }

   public function product($id = null){
     $cover = TableRegistry::get('Covers');
     $product = TableRegistry::get('Products');

     $banners = $cover->find()->where("pages ='PRODUCT' OR pages = 'ALL' AND status = '1'");
     $products = $product->find()->where("status = '1'");

     $this->set(compact('banners','products'));
   }

   public function portfolio(){
     $cover = TableRegistry::get('Covers');
     $product = TableRegistry::get('Products');

     $banners = $cover->find()->where("pages ='PORTFOLIO' OR pages = 'ALL' AND status = '1'");
     $products = $product->find()->where("status = '1'");

     $this->set(compact('banners','products'));
   }

   public function gallery(){
     $cover = TableRegistry::get('Covers');
     $album = TableRegistry::get('Albums');

     $banners = $cover->find()->where("pages ='GALLERY' OR pages = 'ALL' AND status = '1'");
     $albums = $album->find();

     $this->set(compact('banners','akhir','albums'));
   }

   public function contact(){
     $cover = TableRegistry::get('Covers');

     $banners = $cover->find()->where("pages ='CONTACT' OR pages = 'ALL' AND status = '1'");

     $this->set(compact('banners'));
      $contact = TableRegistry::get('Contents');

      $alamat = $contact->find()->where("page = 'kontak' AND position = 'alamat' AND status = '1'");
      $no_kan = $contact->find()->where("page = 'kontak' AND position = 'tlpn1' AND status = '1'");
      $no_sel = $contact->find()->where("page = 'kontak' AND position = 'tlpn2' AND status = '1'");
      $email = $contact->find()->where("page = 'kontak' AND position = 'email' AND status = '1'");
      $cont = $contact->find()->where("page = 'kontak' AND position = 'intro' AND status = '1'");

      // --------------------
      if ($this->request->is('post')) {
          $name = $this->request->data('name');
          $email = $this->request->data('email');
          $password = $this->request->data('password');
          $subjek = $this->request->data('subjek');
          $pesan = $this->request->data('pesan');

          Email::configTransport('gmail', [
            'host' => 'ssl://smtp.gmail.com',
            'port' => 465,
            'username' => $email,
            'password' => $password,
            'className' => 'Smtp'
          ]);

          $emails = new Email();
          $emails->transport('gmail');
          $emails->setFrom($email, $name)
              ->setTo('muhammadsopian001@gmail.com')
              ->subject($subjek)
              ->send($pesan);


          // pr($email); die;

      }


      $this->set(compact('alamat', 'no_kan', 'no_sel', 'email', 'cont'));
   }

   public function detail(){
     $cover = TableRegistry::get('Covers');
     $product = TableRegistry::get('Products');
     $picture = TableRegistry::get('Pictures');
     $album = TableRegistry::get('Albums');

     $url = explode('=',Router::url($this->request->here(), true));
     if(empty($url[1])){
       $this->Flash->error(__('404'));
       $this->redirect(['action'=>'product']);
    }else{
      $preid = explode('&', $url[1]);
      $pages = $url[2];

       if($pages == "product"){
         $product_id = $preid[0];
         $details = $product->find()->where("status = '1' AND id = '$product_id'");
         $banners = $cover->find()->where("pages ='PRODUCT' OR pages = 'ALL' AND status = '1'");
       }else if($pages == "gallery"){
         $gallery_id = $preid[0];
         $details = $picture->find()->where("album_id = '$gallery_id'");
         $banners = $cover->find()->where("pages ='GALLERY' OR pages = 'ALL' AND status = '1'");
         $albums = $album->find()->where("id = '$gallery_id'");
       }else{
         $this->viewBuilder()->setLayout('error404');
       }
    }

     $this->set(compact('banners','details','pages','albums'));

   }
}
