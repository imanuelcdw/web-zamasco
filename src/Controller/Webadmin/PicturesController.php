<?php
namespace App\Controller\Webadmin;

use App\Controller\AppController;
use cake\Routing\Router;

/**
 * Pictures Controller
 *
 * @property \App\Model\Table\PicturesTable $Pictures
 *
 * @method \App\Model\Entity\Picture[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PicturesController extends AppController
{
  public function initialize()
  {
      parent::initialize();
      if(php_sapi_name() !== 'cli'){
          $this->Auth->allow(['index','add','delete','edit','view']);
      }

  }

  function beforeFilter(\Cake\Event\Event $event){
      parent::beforeFilter($event);

      if(isset($this->Security) && $this->request->isAjax() && ($this->action = 'index' || $this->action = 'delete')){

          $this->Security->config('validatePost',false);
          //$this->getEventManager()->off($this->Csrf);

      }

  }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
      if($this->request->is('ajax')){
          $source = $this->Pictures;
          $searchAble = [
              'Pictures.id',
              'Pictures.name',
              'Pictures.path',
              'Albums.album'
          ];
          $data = [
              'source'=>$source,
              'searchAble' => $searchAble,
              'defaultField' => 'Pictures.id',
              'defaultSort' => 'desc',
              'defaultSearch' => [
                  // [
                  //     'keyField' => 'group_id',
                  //     'condition' => '=',
                  //     'value' => 1
                  // ]
              ],
              'contain' => ['Albums']

          ];
          $asd   = $this->Datatables->make($data);
          //$this->set('data', $asd);
          $data = $asd['data'];
          $meta = $asd['meta'];
          $this->set('data',$data);
          $this->set('meta',$meta);
          $this->set('_serialize',['data','meta']);
      }else{
          $titleModule = "Picture";
          $titlesubModule = "List Content";
          $breadCrumbs = [
              Router::url(['action' => 'index']) => $titlesubModule
          ];
          $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
      }
    }

    /**
     * View method
     *
     * @param string|null $id Picture id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $picture = $this->Pictures->get($id, [
            'contain' => ['Albums']
        ]);

        $this->set('picture', $picture);
        $titleModule = "Picture";
        $titlesubModule = "View ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $picture = $this->Pictures->newEntity();
        if ($this->request->is('post')) {
            $picture = $this->Pictures->patchEntity($picture, $this->request->getData());

            if ($this->Pictures->save($picture)) {
                $this->Flash->success(__('The picture has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The picture could not be saved. Please, try again.'));
        }
        $albums = $this->Pictures->Albums->find('list', ['limit' => 200]);
        $this->set(compact('picture', 'albums'));
        $titleModule = "Pictures";
        $titlesubModule = "Create ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('titleModule', 'breadCrumbs', 'titlesubModule'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Picture id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $picture = $this->Pictures->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $picture = $this->Pictures->patchEntity($picture, $this->request->getData());
            if ($this->Pictures->save($picture)) {
                $this->Flash->success(__('The picture has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The picture could not be saved. Please, try again.'));
        }
        $albums = $this->Pictures->Albums->find('list', ['limit' => 200]);
        $this->set(compact('picture', 'albums'));
        $titleModule = "Pictures";
        $titlesubModule = "Edit ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'edit',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Picture id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
      $this->request->allowMethod(['post', 'delete']);
      $picture = $this->Pictures->get($id);

      if ($this->Pictures->delete($picture)) {
          $code = 200;
          $message = __('The picture has been deleted.');
          $status = 'success';
          rmdir();
      } else {
          $code = 99;
          $message = __('The picture could not be deleted. Please, try again.');
          $status = 'error';
      }
      if($this->request->is('ajax')){
          $this->set('code',$code);
          $this->set('message',$message);
          $this->set('_serialize',['code','message']);
      }else{
          $this->Flash->{$status}($message);
          return $this->redirect(['action' => 'index']);
      }
    }
}
